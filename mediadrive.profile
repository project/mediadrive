<?php
/**
 * @file
 * Enables modules and site configuration for a Media Drive site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function mediadrive_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  // Acquia features
  $form['server_settings']['acquia_description'] = array(
    '#type' => 'fieldset',
    '#title' => st('Acquia'),
    '#description' => st('The !an can supplement the functionality of Media Drive by providing enhanced site search (faceted search, content recommendations, content biasing, multi-site search, and others using the Apache Solr service), spam protection (using the Mollom service), and more.  A free 30-day trial is available.', array('!an' => l(t('Acquia Network'), 'http://acquia.com/products-services/acquia-network', array('attributes' => array('target' => '_blank'))))),
    '#weight' => -11,
  );
  $form['server_settings']['enable_acquia_connector'] = array(
    '#type' => 'checkbox',
    '#title' => 'Use Acquia Network Connector',
    '#default_value' => 1,
    '#weight' => -10,
    '#return_value' => 1,
  );
  $form['server_settings']['acquia_connector_modules'] = array(
    '#type' => 'checkboxes',
    '#title' => 'Acquia Network Connector Modules',
    '#options' => array(
      'acquia_agent' => 'Acquia Agent',
      'acquia_search' => 'Acquia Search',
      'acquia_spi' => 'Acquia SPI',
    ),
    '#default_value' => array(
      'acquia_agent',
      'acquia_spi',
    ),
    '#weight' => -9,
    '#states' => array(
      'visible' => array(
        ':input[name="enable_acquia_connector"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['#submit'][] = 'mediadrive_check_acquia_connector';

}

/**
 * Implements hook_install_tasks().
 */
function mediadrive_install_tasks() {
  $acquia_connector = variable_get('mediadrive_install_acquia_connector', FALSE);
  $tasks = array(
    'mediadrive_acquia_connector_enable' => array(
      'display' => FALSE,
      'type' => '',
      'run' => $acquia_connector ? INSTALL_TASK_RUN_IF_NOT_COMPLETED : INSTALL_TASK_SKIP,
    ),
    'mediadrive_connector_form' => array(
      'display_name' => st('Set up Connection to the MediaMosa Server'),
      'type' => 'form',
    ),

    '_mediadrive_setup_wysiwyg' => array(
      'run' => INSTALL_TASK_RUN_IF_REACHED,
    ),

    '_mediadrive_setup_final_tasks' => array(
      'run' => INSTALL_TASK_RUN_IF_REACHED,
    ),
  );
  return $tasks;
}

/**
 * Implements hook_update_projects_alter().
 */
function mediadrive_update_projects_alter(&$projects) {
  // Enable update status for the Media Drive profile.
  $modules = system_rebuild_module_data();
  // The module object is shared in the request, so we need to clone it here.
  $mediadrive = clone $modules['mediadrive'];
  $mediadrive->info['hidden'] = FALSE;
  _update_process_info_list($projects, array('mediadrive' => $mediadrive), 'module', TRUE);
}


/**
 * Implements hook_install_tasks_alter().
 */
function mediadrive_install_tasks_alter(&$tasks, $install_state) {
  global $install_state;

  // Skip profile selection step.
  $tasks['install_select_profile']['display'] = FALSE;

  // Skip language selection install step and default language to English.
  $tasks['install_select_locale']['display'] = FALSE;
  $tasks['install_select_locale']['run'] = INSTALL_TASK_SKIP;
  $install_state['parameters']['locale'] = 'en';

}

/**
 * Check if the Acquia Connector box was selected.
 */
function mediadrive_check_acquia_connector($form_id, &$form_state) {
  $values = $form_state['values'];
  if (isset($values['enable_acquia_connector']) && $values['enable_acquia_connector'] == 1) {
    $options = array_filter($values['acquia_connector_modules']);
    variable_set('mediadrive_install_acquia_connector', TRUE);
    variable_set('mediadrive_install_acquia_modules', array_keys($options));
  }
}

/**
 * Enable Acquia Connector module if selected on site configuration step.
 */
function mediadrive_acquia_connector_enable() {
  $modules = variable_get('mediadrive_install_acquia_modules', array());
  if (!empty($modules)) {
    module_enable($modules, TRUE);
    mediadrive_clear_messages();
  }
}

/**
 * Setting up form mediamosa connector.
 */
function mediadrive_connector_form() {
  $form['connection'] = array(
    '#type' => 'fieldset',
    '#title' => t('Connection settings to MediaMosa REST interface'),
    '#description' => t("Enter MediaMosa Application login (subscription) and REST interface URL that will ba associated with your administrative account. Leave empty if you do not know the client application login. As a Multitenant system, you can have multiple subscriptions to accounts in the system. You will need to previously create a client-account in the MediaMosa server; Login as admin and goto MediaMosa->Configuration->Client applications and press on the tab 'add'. You can skip this step and setup the connector later."),
    '#collapsible' => TRUE,
    '#weight' => -5,
  );
  $form['connection']['mediamosa_connector_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('Enter the URL of the REST interface you want to connect to.'),
    '#required' => FALSE,
    '#default_value' => variable_get('mediamosa_connector_url', ''),
  );
  $form['connection']['mediamosa_connector_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Enter the MediaMosa user name that needs to connect.'),
    '#required' => FALSE,
    '#default_value' => variable_get('mediamosa_connector_username', ''),
  );
  $form['connection']['mediamosa_connector_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t("Password might be required to login, is also called 'shared key'."),
    '#required' => FALSE,
    '#default_value' => variable_get('mediamosa_connector_password', ''),
  );
  $form['connection']['mediadrive_multitenant_site'] = array(
    '#type' => 'radios',
    '#title' => st('MediaDrive Tenancy Mode'),
    '#required' => TRUE,
    '#default_value' => 1,
    '#options' => array(
      0 => t('Single-Tenant'),
      1 => t('Multi-Tenant'),
    ),
    '#description' => st('Enable Single/Multi tenancy for this Site. You can switch between single/multi tenant modes from the administrative settings.'),
  );
  $form['other']['check_connection'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check the connection before saving my connection settings.'),
    '#description' => t('Enable this checkbox to verify the entered login. If will return to this form when login failed.'),
    '#default_value' => TRUE,
  );

  $form['continue'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}

/**
 * Validation of mediamosa connector form.
 */
function mediadrive_connector_form_validate(&$form_state, $form) {

  // Values.
  $values = $form['values'];

  if (!empty($values['mediamosa_connector_url']) && !valid_url($values['mediamosa_connector_url'], TRUE)) {
    form_set_error('mediamosa_connector_url', t('The URL is not well formatted.'));
  }

  if (!empty($values['check_connection']) && (!empty($values['mediamosa_connector_url']) || !empty($values['mediamosa_connector_username']) || !empty($values['mediamosa_connector_password']))) {

    if (empty($values['mediamosa_connector_url'])) {
      form_set_error('mediamosa_connector_url', t('Value is required'));
    }
    if (empty($values['mediamosa_connector_username'])) {
      form_set_error('mediamosa_connector_username', t('Value is required'));
    }
    if (empty($values['mediamosa_connector_password'])) {
      form_set_error('mediamosa_connector_password', t('Value is required'));
    }

    // No errors? Then try to connect.
    if (!form_get_errors()) {
      if (!_mediamosa_connector_check_connection($values['mediamosa_connector_username'], $values['mediamosa_connector_password'], $values['mediamosa_connector_url'])) {
        form_set_error('', t('Unable to login'));
        drupal_set_message(t('Login failed, please check your input'), 'error');
      }
      else {
        drupal_set_message(t('Login successful, your MediaMosa connector has been setup.'));
      }
    }
  }
}

/**
 * Submit form mediamosa connector.
 */
function mediadrive_connector_form_submit($form, &$form_state) {
  // Enable multi-tenancy and set up mediamosa connector variables to allow for
  // sigle-tenancy, in case administrator decides to switch.
  variable_set('mediadrive_multitenant_site', $form_state['values']['mediadrive_multitenant_site']);
  variable_set('mediamosa_connector_url', $form_state['values']['mediamosa_connector_url']);
  variable_set('mediamosa_connector_username', $form_state['values']['mediamosa_connector_username']);
  variable_set('mediamosa_connector_password', $form_state['values']['mediamosa_connector_password']);
  // Create the initial subscription and link it to the admin account.
  $node = new stdClass();
  // Admin.
  $node->uid = 1;
  // Published.
  $node->status = 1;
  // On front page.
  $node->promote = 1;
  $node->type = 'subscription';
  // $node->locked = 0;
  // $node->has_title = 1;
  // $node->has_body = 1;
  $node->title = 'Default Subscription';
  $node->language = LANGUAGE_NONE;
  $node->field_active[$node->language][0]['value'] = 1;
  $node->field_mediamosa_connector_user[$node->language][0]['value'] = $form_state['values']['mediamosa_connector_username'];
  $node->field_mediamosa_connector_pass[$node->language][0]['value'] = $form_state['values']['mediamosa_connector_password'];
  $node->field_mediamosa_connector_url[$node->language][0]['value'] = $form_state['values']['mediamosa_connector_url'];
  $node->field_mediadrive_tos_status[$node->language][0]['value'] = 0;
  node_save($node);

  // Link the default subscription to the admin user.
  $node_ref = new stdClass();
  // Admin.
  $node_ref->uid = 1;
  // Published.
  $node_ref->status = 1;
  // On front page.
  $node_ref->promote = 1;
  $node_ref->type = 'user_reference';
  $node_ref->title = 'Default Subscription - Admin User';
  $node_ref->language = LANGUAGE_NONE;
  $node_ref->field_user_reference_sub[$node_ref->language][0]['nid'] = $node->nid;
  $node_ref->field_user_reference_type[$node_ref->language][0]['value'] = 'primary';
  $node_ref->field_user_reference_user[$node_ref->language][0]['uid'] = 1;
  node_save($node_ref);


  // Saving an initial Subscription Settings for this account.
  $node_set = new stdClass();
  // Obtaining transcoding profile information from the mediamosa client_id
  // provided by the user.
  $transcode_profiles = _mediadrive_multitenant_settings_get_transcoding_profiles($form_state['values']);
  $default_profile = reset($transcode_profiles['default']);
  $transcoding_profiles = implode(',', array_keys($transcode_profiles['profiles']));
  // Admin.
  $node_set->uid = 1;
  // Published.
  $node_set->status = 1;
  // On front page.
  $node_set->promote = 0;
  $node_set->type = 'subscription_settings';
  $node_set->title = 'Subscription Settings for Default Subscription';
  $node_set->language = LANGUAGE_NONE;
  $node_set->field_settings_still_type[$node_set->language][0]['value'] = 'NONE';
  $node_set->field_settings_still_size[$node_set->language][0]['value'] = '352x288';
  $node_set->field_settings_still_h_padding[$node_set->language][0]['value'] = '0';
  $node_set->field_settings_still_v_padding[$node_set->language][0]['value'] = '0';
  $node_set->field_settings_still_time_code[$node_set->language][0]['value'] = '10';
  $node_set->field_settings_still_per_file[$node_set->language][0]['value'] = 3;
  $node_set->field_settings_still_every_sec[$node_set->language][0]['value'] = 5;
  $node_set->field_settings_still_start_time[$node_set->language][0]['value'] = 0;
  $node_set->field_settings_still_end_time[$node_set->language][0]['value'] = 9999999;
  $node_set->field_settings_transcode_default[$node_set->language][0]['value'] = $default_profile;
  $node_set->field_settings_transcode_allowed[$node_set->language][0]['value'] = $transcoding_profiles;
  $node_set->field_subscription_reference[$node_set->language][0]['nid'] = $node->nid;
  node_save($node_set);
}

/**
 * Setup wysiwyg editor.
 */
function _mediadrive_setup_wysiwyg() {
  $format = 'full_html';
  $editor = 'ckeditor';
  $settings = 'a:20:{s:7:"default";i:1;s:11:"user_choose";i:0;s:11:"show_toggle";i:1;s:5:"theme";s:8:"advanced";s:8:"language";s:2:"en";s:7:"buttons";a:1:{s:6:"drupal";a:1:{s:5:"media";i:1;}}s:11:"toolbar_loc";s:3:"top";s:13:"toolbar_align";s:4:"left";s:8:"path_loc";s:6:"bottom";s:8:"resizing";i:1;s:11:"verify_html";i:1;s:12:"preformatted";i:0;s:22:"convert_fonts_to_spans";i:1;s:17:"remove_linebreaks";i:1;s:23:"apply_source_formatting";i:0;s:27:"paste_auto_cleanup_on_paste";i:0;s:13:"block_formats";s:32:"p,address,pre,h2,h3,h4,h5,h6,div";s:11:"css_setting";s:5:"theme";s:8:"css_path";s:0:"";s:11:"css_classes";s:0:"";}';

  db_query(
    "INSERT INTO {wysiwyg} SET editor = :editor, settings = :settings, format = :format",
    array(
      ':editor' => $editor,
      ':settings' => $settings,
      ':format' => $format,
    )
  );
}

/**
 * Performs final configuration tasks for Media Drive.
 */
function _mediadrive_setup_final_tasks() {
  // Set the site front-page.
  variable_set('site_frontpage', 'assets');
  // Configuring purl.
  $purl_types = array(
    'querystring' => 'querystring',
    'path' => 0,
    'pair' => 0,
    'subdomain' => 0,
    'domain' => 0,
    'extension' => 0,
    'useragent' => 0,
  );
  variable_set('purl_types', $purl_types);
  variable_set('purl_method_mediadrive_subscription', 'querystring');
  variable_set('purl_method_mediadrive_subscription_key', 's');
  // Enabling mediadrive_omega theme.
  theme_enable(array('mediadrive_omega'));
  // Updating the main-menu.
  $main_menu = menu_load_links('main-menu');
  foreach ($main_menu as $item) {
    if ($item['link_title'] == 'Home') {
      menu_link_delete($item['mlid']);
    }
    elseif ($item['link_path'] == 'assets') {
      $item['link_title'] = 'Browse';
      menu_link_save($item);
    }
  }
  menu_rebuild();
}

/**
 * Clear all 'notification' type messages that may have been set.
 */
function mediadrive_clear_messages() {
  drupal_get_messages('status', TRUE);
  drupal_get_messages('completed', TRUE);
  // Migrate adds its messages under the wrong type, see #1659150.
  drupal_get_messages('ok', TRUE);
}
