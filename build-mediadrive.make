api = 2
core = 7.x

; Include Drupal core and any core patches.
includes[] = drupal-org-core.make

; Download the mediadrive install profile
projects[mediadrive][type] = profile
projects[mediadrive][download][type] = git
projects[mediadrive][download][url] = http://git.drupal.org/project/mediadrive.git
projects[mediadrive][download][full_version] = 7.x-dev
