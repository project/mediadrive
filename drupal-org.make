api = 2
core = 7.x

; Contrib modules.
projects[acquia_connector][type] = "module"
projects[acquia_connector][subdir] = "contrib"
projects[acquia_connector][version] = "2.11"

projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.3"

projects[delta][type] = "module"
projects[delta][subdir] = contrib
projects[delta][version] = "3.0-beta11"

projects[features][type] = "module"
projects[features][subdir] = "contrib"
projects[features][version] = "1.0"

projects[field_group][type] = "module"
projects[field_group][subdir] = "contrib"
projects[field_group][version] = "1.1"

projects[file_entity][type] = "module"
projects[file_entity][subdir] = "contrib"
projects[file_entity][version] = "2.0-unstable7"

projects[styles][type] = "module"
projects[styles][subdir] = "contrib"
projects[styles][version] = "2.x-dev"

projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = contrib
projects[jquery_update][version] = "2.3"
projects[jquery_update][patch][] = "http://drupal.org/files/jquery_update_ui_1.8.24.patch"

projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"
projects[libraries][version] = "2.0-alpha2"

projects[media][type] = "module"
projects[media][subdir] = "contrib"
projects[media][version] = "2.0-unstable7"

projects[oauth][type] = "module"
projects[oauth][subdir] = contrib
projects[oauth][version] = "3.1"

; Previous versions of purl do not work with this.
projects[purl][subdir] = "contrib"
projects[purl][type] = "module"
projects[purl][download][type] = "git"
projects[purl][download][full_version] = "1.0-beta11-dev"
projects[purl][download][url] = "http://git.drupal.org/project/purl.git"
projects[purl][download][revision] = "469e8668685de4a573e06b29742f6de41771e02c"

projects[r4032login][type] = "module"
projects[r4032login][subdir] = contrib
projects[r4032login][version] = "1.5"

projects[references][type] = "module"
projects[references][subdir] = contrib
projects[references][version] = "2.1"

projects[field_group][type] = "module"
projects[field_group][subdir] = contrib
projects[field_group][version] = "1.2"

projects[services][type] = "module"
projects[services][subdir] = contrib
projects[services][version] = "3.5"

projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[views][version] = "3.7"

projects[wysiwyg][type] = "module"
projects[wysiwyg][subdir] = "contrib"
projects[wysiwyg][version] = "2.2"

; Themes
projects[omega][type] = "theme"
projects[omega][subdir] = "custom"
projects[omega][version] = "3.1"

; Custom modules

; MediaMosa modules
projects[mediamosa_sdk][subdir] = "custom"
projects[mediamosa_sdk][type] = "module"
projects[mediamosa_sdk][download][type] = "git"
projects[mediamosa_sdk][download][full_version] = "7.x-3.5"
projects[mediamosa_sdk][download][url] = "http://github.com/mediamosa/mediamosa-sdk.git"
projects[mediamosa_sdk][download][revision] = "ac5de4c672932a4d3d7f235ef1276513ebf115e5"

projects[mediamosa_sb][subdir] = "custom"
projects[mediamosa_sb][type] = "module"
projects[mediamosa_sb][download][type] = "git"
projects[mediamosa_sb][download][full_version] = "7.x-1.x-dev"
projects[mediamosa_sb][download][url] = "http://github.com/acquia/mediamosa_sb.git"
projects[mediamosa_sb][patch][] = "http://drupal.org/files/amd-001_metadata_options.patch"
projects[mediamosa_sb][patch][] = "http://drupal.org/files/amd-002_ownerid_stillfalse.patch"
projects[mediamosa_sb][patch][] = "http://drupal.org/files/amd-003_create_still_params.patch"
projects[mediamosa_sb][patch][] = "http://drupal.org/files/amd-004_collection_public_assign.patch"
projects[mediamosa_sb][patch][] = "http://drupal.org/files/mediamosa-001_create_mediafile_still_asset.patch"
projects[mediamosa_sb][patch][] = "http://drupal.org/files/mediamosa-002-2_advanced_search.patch"

projects[mediamosa_ck][subdir] = "custom"
projects[mediamosa_ck][type] = "module"
projects[mediamosa_ck][download][type] = "git"
projects[mediamosa_ck][download][full_version] = "7.x-3.5"
projects[mediamosa_ck][download][url] = "http://github.com/mediamosa/mediamosa-ck.git"
projects[mediamosa_ck][download][revision] = "d021d7519f4feb0500b81bae0f10f2cd0ce7b482"
projects[mediamosa_ck][patch][] = "http://drupal.org/files/mediamosa-002-1_advanced_search.patch"
projects[mediamosa_ck][patch][] = "http://drupal.org/files/mediamosa-003_malformed_cql.patch"
projects[mediamosa_ck][patch][] = "http://drupal.org/files/mediamosa-004_image_flush.patch"
projects[mediamosa_ck][patch][] = "http://drupal.org/files/mediamosa-005_still_image_path.patch"
projects[mediamosa_ck][patch][] = "http://drupal.org/files/mediamosa-006_delete_profile.patch"
projects[mediamosa_ck][patch][] = "http://drupal.org/files/mediamosa-007_xml_collection.patch"
projects[mediamosa_ck][patch][] = "http://drupal.org/files/amd-006_multitenancy.patch"

projects[mediamosa_sb_feature][subdir] = "custom"
projects[mediamosa_sb_feature][type] = "module"
projects[mediamosa_sb_feature][download][type] = "git"
projects[mediamosa_sb_feature][download][full_version] = "7.x-1.1"
projects[mediamosa_sb_feature][download][url] = "http://github.com/acquia/mediamosa_sb_feature.git"

; Libraries
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.zip"
libraries[ckeditor][type] = "libraries"
