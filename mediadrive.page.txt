You have completed the installation of the Acquia Media Drive.

But before you proceed, setup the cron job for this website first. You can read how to configure your cron on page http://drupal.org/cron/.

We recommend that you check the Acquia Media Drive settings on !url_mmck_settings.

During the setup you had the option to setup the an the MediaMosa Connector for one tenant. If you have skipped it during setup, you can set it up on !url_mediamosa_connector.

Once MediaMosa Connector is setup, visit the Acquia Media Drive settings page and use start using it. 

To integrate Media Drive videos in your HTML editor, we have setup wysiwyg module with default settings. You can review these settings on !url_wysiwyg.

blah blah blah...
