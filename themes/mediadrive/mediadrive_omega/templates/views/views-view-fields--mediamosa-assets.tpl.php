<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<div class="mediadrive-asset-item">
  <div class="mediadrive-asset-image">
    <?php print $fields['still_url']->content; ?>
  </div>
  <div class="mediadrive-asset-hover-wrapper">
    <div class="mediadrive-asset-hover">
      <div class="mediadrive-asset-hover-field mediadrive-asset-title"><?php print $fields['title']->content; ?>
        <span class="mediadrive-asset-hover-field mediadrive-asset-duration"><?php print isset($fields['mediafile_duration']->content) ? $fields['mediafile_duration']->content : NULL; ?></span>
      </div>
      <div class="mediadrive-asset-hover-field mediadrive-asset-small"><?php print t('Uploaded on') .' ' . $fields['videotimestamp']->content; ?></div>
      <div class="mediadrive-asset-hover-field mediadrive-asset-edit"><?php print l(t('edit'), 'asset/edit/' . $fields['asset_id']->raw)?></div>
    </div>
  </div>
</div>
