<?php
/**
 * @file views-view-fields.tpl.php
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>

<div class="mediadrive-asset">
  <ul class="mediadrive-item-navigation mediadrive-align-navigation-with-title">
    <?php if ($fields['access_edit']->raw == TRUE) : ?>
      <li><span><?php print l(t('Edit'), 'asset/edit/' . $fields['asset_id']->raw, array('attributes' => array('class' => 'mediadrive-simple-button'))); ?></span></li>
    <?php endif; ?>
  </ul>
  <div class="first-column-wrapper column-wrapper">
    <div class="first-column">
      <div class="asset-section basics">
        <h2 class="asset-section-title"><?php print t('Basics'); ?></h2>
        <div class="asset-subsection title">
          <h3 class="asset-subsection-title"><?php print t('Title'); ?></h3>
          <p class="asset-subsection-value"><?php print $fields['title']->content; ?></p>
        </div>
        <?php if($fields['description']->raw != NULL) : ?>
          <div class="asset-subsection desc">
            <h3 class="asset-subsection-title"><?php print t('Description'); ?></h3>
            <p class="asset-subsection-value"><?php print $fields['description']->content; ?></p>
          </div>
        <?php endif; ?>
        <div class="asset-subsection date">
          <h3 class="asset-subsection-title"><?php print t('Date added'); ?></h3>
          <p class="asset-subsection-value"><?php print $fields['videotimestamp']->content; ?></p>
        </div>
        <div class="asset-subsection author">
          <h3 class="asset-subsection-title"><?php print t('Author'); ?></h3>
          <p class="asset-subsection-value"><?php print $fields['owner_id']->content; ?></p>
        </div>
        <div class="asset-subsection freetags">
          <h3 class="asset-subsection-title"><?php print t('Tags') . " (" . l(t("view all tags"), 'asset/alltags') . ")" ; ?></h3>
          <p class="asset-subsection-value"><?php print $fields['asset_id']->handler->view->result[0]->freetags; ?></p>
        </div>
        <?php if(isset($fields['favorite']) && $fields['favorite']->raw != NULL) : ?>
          <div class="asset-subsection featured">
            <p class="asset-subsection-value"><?php print t('Featured'); ?></p>
          </div>
        <?php endif; ?>
        <?php if($fields['collections']->raw != NULL) : ?>
          <div class="asset-subsection collections">
            <h3 class="asset-subsection-title"><?php print t('Collections'); ?></h3>
            <p class="asset-subsection-value"><?php print $fields['collections']->content; ?></p>
          </div>
        <?php endif; ?>
      </div>
      <div class="asset-section metadata">
        <h2 class="asset-section-title"><?php print t('Meta data'); ?></h2>
        <fieldset class="collapsed collapsible mediadrive-fieldset mediadrive-fieldset-basics">
          <legend>
             <span class="fieldset-legend"><?php print t('Dublin Core'); ?><span class="summary"></span></span>
          </legend>
          <div class="fieldset-wrapper">
            <?php print $fields['metadata_dc']->content; ?>
          </div>
        </fieldset>
        <fieldset class="collapsed collapsible mediadrive-fieldset mediadrive-fieldset-basics">
          <legend>
             <span class="fieldset-legend"><?php print t('Qualified Dublin Core'); ?><span class="summary"></span></span>
          </legend>
          <div class="fieldset-wrapper">
            <?php print $fields['metadata_qdc']->content; ?>
          </div>
        </fieldset>
        <fieldset class="collapsed collapsible mediadrive-fieldset mediadrive-fieldset-basics">
          <legend>
             <span class="fieldset-legend"><?php print t('Technical Metadata'); ?><span class="summary"></span></span>
          </legend>
          <div class="fieldset-wrapper">
            <?php print $fields['technical_metadata']->content; ?>
          </div>
        </fieldset>
        <!-- <div class="asset-subsection subject"> -->
        <!--   <h3 class="asset-subsection-title"><?php print t('Subject'); ?></h3> -->
        <!--   <p class="asset-subsection-value"><?php print $fields['title']->content; ?></p> -->
        <!-- </div> -->
        <?php if(isset($fields['dc_language']) && $fields['dc_language']->raw != NULL) : ?>
          <div class="asset-subsection language">
            <h3 class="asset-subsection-title"><?php print t('Language'); ?></h3>
            <p class="asset-subsection-value"><?php print $fields['dc_language']->content; ?></p>
          </div>
        <?php endif; ?>
      </div>
    <!--
      <div class="asset-section licensing">
        <h2 class="asset-section-title"><?php print t('Licensing'); ?></h2>
        <div class="asset-subsection license">
          <h3 class="asset-subsection-title"><?php print $fields; ?></h3>
          <p class="asset-subsection-value"><?php print $fields; ?></p>
        </div>
      </div>
    -->
      <div class="asset-section privacy">
        <h2 class="asset-section-title"><?php print t('Privacy'); ?></h2>
        <?php print $fields['acl_info']->content; ?>
      </div>
    </div>
  </div>

  <div class="second-column-wrapper column-wrapper clearfix">
    <div class="second-column">
      <div class="asset-section media">
        <div class="asset-subsection media-shot">
          <p class="asset-subsection-value"><?php print $fields['player']->content; ?></p>
        </div>
        <div class="asset-subsection views">
          <p class="asset-subsection-value">
          <?php print ((int) $fields['played']->content == 1) ?
          '<span class="view-number">' . $fields['played']->content . '</span><span class="view-suffix"> view</span>' :
          '<span class="view-number">' . $fields['played']->content . '</span><span class="view-suffix"> views</span>'; ?>
          </p>
        </div>
      </div>
      <div class="asset-section transcoding">
        <h2 class="asset-section-title"><?php print t('Transcoding'); ?></h2>
        <div class="asset-subsection transcoding-widget">
          <div class="asset-subsection-value"><?php print $fields['transcodes']->content; ?></div>
        </div>
      </div>
      <div class="asset-section embed">
        <h2 class="asset-section-title"><?php print t('Embed'); ?></h2>
        <div class="asset-subsection embed-code">
          <p class="asset-subsection-value"><?php print $fields['embed']->content; ?></p>
        </div>
        <div class="asset-subsection subject">
          <h3 class="asset-subsection-title"><?php print t('Files'); ?></h3>
          <p class="asset-subsection-value"><?php print $fields['mediafiles']->content; ?></p>
        </div>
        <div class="asset-subsection thumbnail">
          <h3 class="asset-subsection-title"><?php print t('Thumbnail'); ?></h3>
          <p class="asset-subsection-value"><?php print $fields['still_url']->content; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>
