(function($) {
  // Behavior to make the filter selects look better.
  Drupal.behaviors.mediadriveBetterLookingFilterSelects = {
    attach: function (context) {
      var block = $('.block-views-exp-mediamosa-assets-page'), form = block.find('form'), selects = block.find('select');
      // Add a class to selects
      selects.addClass('mediadrive-onchange-submit');
      // Hide submit button and trigger it on change of any of the selects.
      block.find('.form-submit').hide();

      // Send form on select change.
      selects.change(function(e){
        $(this).removeClass('mediadrive-onchange-submit');
        form.submit();
        selects.attr('disabled', 'disabled');
      });

    }
  };
})(jQuery);


