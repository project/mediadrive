(function($){

  Drupal.behaviors.mediadrive_still_selection = {

    attach: function (context, settings) {

      function media_mediamosa_still_hideshow_item(name, show) {
        obj = document.getElementById(name);
        if (obj) {
          obj.style.display = show ? "block" : "none";
        }
      }

      function updated_media_mediadrive_still_hide_by_type(obj) {
        var items = {};
        var button_message = "Generate stills";
        switch (jQuery(obj).val()) {
          case 'NONE':
            items = {
              'show-media_mediamosa-h_padding': true, // Upload.
              'show-media_mediamosa-v_padding': true, // Upload.
              'show-media_mediamosa-frametime': true, // None.
              'show-media_mediamosa-still-per-mediafile': false, // Normal.
              'show-media_mediamosa-still-every-second': false, // Second.
              'show-media_mediamosa-start-time': false, // Second.
              'show-media_mediamosa-end-time': false, // Second.
              'show-media_mediamosa-file': false, // Upload.
              'show-media_mediamosa-order': false, // Upload.
              'show-media_mediamosa-default': false // Upload.
            };

            button_message = "Generate still";
            break;

          case 'NORMAL':
            items = {
              'show-media_mediamosa-h_padding': true, //Upload
              'show-media_mediamosa-v_padding': true, // Upload
              'show-media_mediamosa-frametime': false, // None
              'show-media_mediamosa-still-per-mediafile': true,
              'show-media_mediamosa-still-every-second': false, // Second.
              'show-media_mediamosa-start-time': true, // Second.
              'show-media_mediamosa-end-time': true, // Second.
              'show-media_mediamosa-file': false, // Upload.
              'show-media_mediamosa-order': false, // Upload.
              'show-media_mediamosa-default': false // Upload.
            };
            break;

          case 'SECOND':
            items = {
              'show-media_mediamosa-h_padding': true, //Upload
              'show-media_mediamosa-v_padding': true, // Upload
              'show-media_mediamosa-frametime': false, // None
              'show-media_mediamosa-still-per-mediafile': false,
              'show-media_mediamosa-still-every-second': true, // Second.
              'show-media_mediamosa-start-time': true, // Second.
              'show-media_mediamosa-end-time': true, // Second.
              'show-media_mediamosa-file': false, // Upload.
              'show-media_mediamosa-order': false, // Upload.
              'show-media_mediamosa-default': false // Upload.
            };
            break;

          case 'SCENE':
            items = {
              'show-media_mediamosa-h_padding': true, //Upload
              'show-media_mediamosa-v_padding': true, // Upload
              'show-media_mediamosa-frametime': false, // None
              'show-media_mediamosa-still-per-mediafile': false,
              'show-media_mediamosa-still-every-second': false, // Second.
              'show-media_mediamosa-start-time': false, // Second.
              'show-media_mediamosa-end-time': false, // Second.
              'show-media_mediamosa-file': false, // Upload.
              'show-media_mediamosa-order': false, // Upload.
              'show-media_mediamosa-default': false // Upload.
            };
            break;

          case 'UPLOAD':
            items = {
              'show-media_mediamosa-file': true, //Upload
              'show-media_mediamosa-h_padding': false, //Upload
              'show-media_mediamosa-v_padding': false, // Upload
              'show-media_mediamosa-frametime': false, // None
              'show-media_mediamosa-still-per-mediafile': false,
              'show-media_mediamosa-still-every-second': false, // Second.
              'show-media_mediamosa-start-time': false, // Second.
              'show-media_mediamosa-end-time': false, // Second.
              'show-media_mediamosa-order': true, // Upload.
              'show-media_mediamosa-default': true // Upload.
            };
            button_message = "Upload still";
            break;
        }

        $('#edit-still-for-old-mediafile').val(button_message);
        Object.keys(items).forEach(function(item){
          media_mediamosa_still_hideshow_item(item, items[item]);
        });
      }

      // Show/Hide the form elements according to still_type selection
      updated_media_mediadrive_still_hide_by_type($('#edit-still-type'));
      // Moving the checkboxes closer to the input boxes.
      $('input[type=checkbox]').each(function () {
        var id = $(this).attr('id');
        var out = id.replace('delete', 'images');
        if (id != 'edit-default') {
          $($(this).parent()).appendTo($('#' + out).parent());
        }
      });
    }
  }
  Drupal.behaviors.mediadriveStillSelectionListings = {
    attach: function (context, settings) {
      var selection_form = $('#mediadrive-still-selection-asset-still-form');
      flagDefaultRadio();
      selection_form.find('.form-type-radio img').click(function(e){
        selection_form.find('.form-type-radio').removeClass('radio-checked');
        $(this).parents('.form-type-radio').addClass('radio-checked');
      });

      function flagDefaultRadio() {
        selection_form.find('.form-type-radio input.form-radio').each(function(e){
          if($(this).is(':checked')) {
            $(this).parent().addClass('radio-checked');
          }
        });
      }
    }
  }
}(jQuery));
