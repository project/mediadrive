<?php

/**
 * @file
 * Form to perform bulk assets deletions.
 */

/**
 * Provides the form for bulk deletion of assets.
 */
function mediadrive_bulk_delete_form($form_state) {
  $form = array();
  $sub = MEDIADRIVEMultitenant::getSubscription();
  $page_limit = 20;
  // Fake the total because we don't know until after the query.
  // We don't wanna do the same REST Call twice.
  // This won't affect anything as the same function is called again
  // to set the real page globals.
  $page0 = pager_default_initialize(10000, $page_limit);
  $page_offset = $page_limit * $page0;

  // $app_id = '1002';
  // $cql = 'app_id=' . $app_id;
  $table_options = array(
    // 'offset' => (isset($page_offset) && $page_offset > 0) ? $page_offset : 1,
    'offset' => isset($page_offset) ? $page_offset : 0,
    'limit' => $page_limit,
    // 'cql' => $cql,
    'hide_empty_assets' => 'FALSE',
    'order_direction' => 'desc',
  );
  // Do the search.
  $response = mediadrive_bulk_delete_search_assets($table_options);
  if (!empty($response)) {
    // Now process the xml into an array for the rows.
    foreach ($response->xml->xpath('items/item') as $item) {
      $asset_id = (string) $item->asset_id;
      $options[$asset_id] = array(
        'asset_id' => array('data' => $asset_id),
        'asset' => array('data' => l($asset_id, 'asset/detail/' . $asset_id, array('attributes' => array('target' => '_blank')))),
        'title' => array('data' => (string) $item->dublin_core->title),
        'is_empty' => array('data' => ((string) $item->is_empty_asset == 'FALSE') ? 'NO' : 'YES'),
        'viewed' => array('data' => (string) $item->viewed),
        'played' => array('data' => (string) $item->played),
        'app_id' => array('data' => (string) $item->app_id),
        'owner_id' => array('data' => (string) $item->owner_id),
        'timestamp' => array('data' => (string) $item->videotimestamp),
        'last_modified' => array('data' => (string) $item->videotimestampmodified),
      );
    }
    $total_obj = $response->xml->xpath('header/item_count_total');
    $total = (string) $total_obj[0];
  }
  // Here we know the real total, then we can set the page globals.
  $page = pager_default_initialize($total, $page_limit);

  $header = array(
    'asset' => array('data' => t('Asset ID')),
    'is_empty' => array('data' => t('Is empty?')),
    'title' => array('data' => t('Title')),
    'viewed' => array('data' => t('Viewed')),
    'played' => array('data' => t('Played')),
    'app_id' => array('data' => t('App ID')),
    'owner_id' => array('data' => t('Owner')),
    'timestamp' => array('data' => t('Timestamp')),
    'last_modified' => array('data' => t('Last Modified')),
  );

  $form['assets'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => (isset($options) && is_array($options)) ? $options : array(),
    '#empty' => t('No items available'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete Selected Assets'),
  );
  $form['pager'] = array('#markup' => theme('pager'));
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function mediadrive_bulk_delete_form_submit($form, $form_state) {
  $assets_deleted = array();
  $assets_not_deleted = array();
  // $user_id = mediamosa_ck::session_user_id();
  // Loop for assets deletion.
  foreach ($form_state['values']['assets'] as $key => $asset_id) {
    if ($key === $asset_id) {
      $owner_id = $form_state['complete form']['assets']['#options'][$key]['owner_id']['data'];
      // We have the power to delete the asset because we have access to it,
      // through the subscription (User belongs to this subscription).
      // even if this user does not own it (owner_id might be different to
      // user_id).
      // TODO: Create a better verification process for deleting assets.
      if (!MediaMosaSbConnectorWrapper::delete_asset($asset_id, $owner_id)) {
        $assets_not_deleted_element = $form_state['complete form']['assets']['#options'][$key]['asset']['data'];
        mediamosa_sb::watchdog_error('Unable to delete asset: ' . $assets_not_deleted_element);
        $assets_not_deleted[] = $assets_not_deleted_element;
      }
      else {
        $assets_deleted[] = $form_state['complete form']['assets']['#options'][$key]['asset']['data'];
      }
    }
  }
  if (count($assets_deleted) > 0) {
    drupal_set_message(t('%number assets deleted: ',
      array(
        '%number' => count($assets_deleted),
      )) . implode(', ', $assets_deleted),
    'status');
  }
  if (count($assets_not_deleted) > 0) {
    drupal_set_message(t('%number assets could not be deleted: ',
      array(
        '%number' => count($assets_not_deleted),
      )) . implode(', ', $assets_not_deleted),
    'warning');
  }
}

/**
 * Search assets. Uses the /asset REST call.
 *
 * @param $options
 *   Array with options for search.
 *
 * @return
 *   The assets contained in a mediamosa_connector_response object.
 */
function mediadrive_bulk_delete_search_assets(array $options = array()) {
  // Setup default values.
  $options += array(
    // Position in result set.
    'offset' => 0,
    // Number of items in result set.
    'limit' => 10,
    // cql search query. Do not provide sortBy in query(!), use order_by and order_direction.
    'cql' => '',
    // Order/sort by field.
    'order_by' => 'videotimestampmodified',
    // Direction either desc or asc.
    'order_direction' => 'asc',
    // Search within these collection(s).
    'coll_id' => array(),

    // acl_realm.
    'acl_realm' => '',
    // acl_domain.
    'acl_domain' => '',
    // acl_group_ids.
    'acl_group_id' => array(),
    // acl_user_id.
    'acl_user_id' => '',
  );

  // CQL search.
  $cql = empty($options['cql']) ? array() : array($options['cql']);

  // Always sortby videotimestampmodified.
  if (!empty($options['order_by'])) {
    $cql[] = 'sortby ' . $options['order_by'] . (!empty($options['order_direction']) ? '/' . ($options['order_direction'] == 'asc' ? 'ascending' : 'descending') : '');
  }
  // Skip these.
  unset($options['order_by']);
  unset($options['order_direction']);

  // Settings for REST call.
  $data = array(
    'cql' => implode(' ', $cql),
    'limit' => $options['limit'],
    'offset' => $options['offset'],
    // Protection to not include assets that don't have mediafiles.
    // But in this case we want to include them.
    // 'hide_empty_assets' => 'TRUE',
    // Get total count, need pager.
    'calculate_total_count' => 'TRUE',
  );

  // Add our options to $data.
  $data = array_merge($options, $data);

  try {
    return mediamosa_ck::request_get_fatal('asset', array('data' => $data));
  }
  catch (Exception $e) {
    mediamosa_ck::watchdog_error('Error search assets. Message: !message.', array('!message' => $e->getMessage()), WATCHDOG_ERROR);
  }

  // Got here, then return FALSE.
  return FALSE;
}
