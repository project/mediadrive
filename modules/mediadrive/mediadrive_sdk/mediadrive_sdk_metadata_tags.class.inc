<?php
/**
 * @file
 * MEDIADRIVE Metadata Tags Class for managing free-tags.
 *
 *  This class provides all the Acquia Media Drive functions to manage
 *  free-tags, or what is called metadata_tags.
 */

/**
 * The class to manage metadata_tags.
 */
class mediadrive_sdk_metadata_tags {
  protected $tags = array();

  /**
   * Implements Class Constructor.
   */
  public function __construct() {
    $this->setTags();
  }

  /**
   * Provides the list of metadata_tags for this client app.
   *
   * @return array
   *   An array that list all the metadata tags for the current client app.
   */
  public function get() {
    return $this->tags;
  }

  /**
   * Creates an array of all metadata_tags for this client app.
   *
   * @param array $options
   *   Defines the POST parameters that will be passed to the API Call.
   *
   * @return array
   *   The list of client application tags.
   */
  protected function setTags($options = array()) {
    $app_tags = array();
    // Listing all the different functions.
    $response = $this->queryTags($options);
    foreach ($response->xml->xpath('items/item') as $item) {
      $app_tags[] = (string) $item->name;
    }
    $this->tags = $app_tags;
  }

  /**
   * Obtains all metadata tags for the current subscription.
   *
   * By default, it will only list all metadata_tags that have been custom
   * created for this client app (subscription).
   *
   * @param array $options
   *   Defines the POST parameters that will be passed to the API Call.
   *
   * @return string
   *   Returns the XML response string after a 'metadata_tag' API call.
   */
  protected function queryTags($options = array()) {
    // Defining the options for the search.
    $data = array_merge(array(
              'include_default_definitions' => 'FALSE',
              'limit' => 200,
            ), $options);

    // Perform the request.
    try {
      return mediamosa_ck::request_get_fatal('metadata_tag', array('data' => $data));
    }
    catch (Exception $e) {
      mediamosa_ck::watchdog_error('Error searching metadata_tags. Message: !message.', array('!message' => $e->getMessage()), WATCHDOG_ERROR);
    }
  }

  /**
   * Creates a new metadata_tag in Media Drive.
   *
   * By default, it will create a metadata_tag property of the type 'char'.
   *
   * @param string $name
   *   The name of the 'tag'.
   * @param string $type
   *   The type of the 'tag'. Accepted values are: 'char', 'int', 'datetime'.
   *
   * @return string,FALSE
   *   The string with the new 'tag name' created or FALSE, if unsuccessful.
   */
  public function createTag($name = NULL, $type = 'char') {
    if (!empty($name)) {
      $data = array(
        'name' => $name,
        'type' => $type,
      );
      try {
        $response = mediamosa_ck::request_post_fatal('metadata_tag/create', array('data' => $data));
        $new_tag = (string) $response->items->item[0]->prop_name;
        $this->tags[] = $new_tag;
        return $new_tag;
      }
      catch (Exception $e) {
        mediamosa_ck::watchdog_error('Error creating metadata_tag \'!name\'. Message: !message.', array(
          '!name' => $name,
          '!message' => $e->getMessage(),
        ), WATCHDOG_ERROR);
      }
    }
    return FALSE;
  }

  /**
   * Extract custom metadata_tags from an 'asset/$asset_id' API call.
   *
   * @param string $asset
   *   This is the XML response string from an 'asset/$asset_id' API call.
   *
   * @return array
   *   Returns the list of metadata_tags for the current asset.
   */
  public static function get_asset_tags($asset) {
    $asset_tags = '';
    // Obtaining the app_id
    $app_id = 'app_' . (string) $asset->items->item[0]->app_id;
    // Loop through fields.
    foreach ((array) $asset->items->item[0]->{"$app_id"} as $metadata_tag => $metadata_value) {
      if (!empty($metadata_value)) {
        $asset_tags .= empty($asset_tags) ? (string) $metadata_value : ', ' . (string) $metadata_value;
      }
    }
    return $asset_tags;
  }

  /**
   * Makes an API call to Media Drive and returns the metadata_tags.
   *
   * @param string $asset_id
   *   The Asset ID to query for free-tags.
   *
   * @return array
   *   An array of all tags for the current asset.
   */
  public function query_asset_tags($asset_id) {
    // Get the asset.
    $asset = MediaMosaSbConnectorWrapper::get_asset($asset_id, array('show_collections' => FALSE));
    if (!$asset) {
      drupal_set_message('Media not found for asset %asset_id.', array('%asset_id' => $asset_id));
      return;
    }
    $asset_tags = self::get_asset_tags($asset);
    return $asset_tags;
  }

  /**
   * Creates many tags from an array (name, type).
   *
   * @param array $names
   *   An array of tag names.
   * @param array $types
   *   An array of types for each name.
   *
   * @return array
   *   The list of tags, including the newly created ones.
   */
  public function createTags($names = array(), $types = array()) {
    if (count($names) != count($types) && count($types) != 0) {
      drupal_set_message('Tag names and types should have the same number of elements.');
      return;
    }
    foreach ($names as $key => $name) {
      $type = isset($types[$key]) ? $types[$key] : 'char';
      $response = $this->createTag($name, $type);
    }
    return $this->tags;
  }

  /**
   * Deletes a metadata_tag in Media Drive.
   *
   * By default it will operate on 'cascade' mode, meaning that it will also
   * delete all metadata if the property still has it. Does not need the
   * property to be empty.
   *
   * @param string $name
   *   The Tag name to delete.
   * @param array $options
   *   Either 'cascade' or ''.
   *
   * @return boolean
   *   TRUE if successful, FALSE otherwise.
   */
  public function deleteTag($name = NULL, $options = array()) {
    if (!empty($name)) {
      $data = array(
        'name' => $name,
        'delete' => 'cascade',
      );
      try {
        $response = mediamosa_ck::request_post_fatal('metadata_tag/delete', array('data' => $data));
        foreach ($this->tags as $key => $tag) {
          if ($name == $tag) {
            unset($this->tags[$key]);
            return TRUE;
          }
        }
      }
      catch (Exception $e) {
        mediamosa_ck::watchdog_error('Error deleting metadata_tag \'!name\'. Message: !message.', array(
          '!name' => $name,
          '!message' => $e->getMessage(),
        ), WATCHDOG_ERROR);
      }
    }
    return FALSE;
  }

  /**
   * Update the metadata for given asset ID.
   *
   * @param string $asset_id
   *   The ID of the asset.
   * @param array $options
   *   The options for the REST call.
   *
   * @return boolean
   *   TRUE if successful, FALSE otherwise.
   */
  public static function update_metadata($asset_id, array $options = array()) {

    // Add action.
    $options = array_merge(array(
      'user_id' => mediamosa_ck::session_user_id(),
    ), $options);

    try {
      // Do request.
      mediamosa_ck::request_post_fatal('asset/' . rawurlencode($asset_id) . '/metadata', array('data' => $options));
    }
    catch (Exception $e) {
      drupal_set_message('Unable to update metadata; ' . $e->getMessage(), 'error');
      return FALSE;
    }
    return TRUE;
  }


  /**
   * Assign metadata tags associated to this particular asset.
   *
   * @param array/string $asset_tags
   *   Comma-separated string or tags array.
   * @param string $asset_id
   *   The asset_id to which the tags will be saved.
   * @param string $action
   *   The action that will be executed for the provided tags list:
   *    - 'append' => Creates new tags (default behavior).
   *    - 'update' => Updates the tags for the provided list.
   *    - 'replace' => Replaces all tags (deletes unselected tags)
   * @param string $user_id
   *   Email address of the user who will modify the metadata.
   *
   * @return boolean
   *   TRUE if successful, FALSE otherwise.
   */
  public function submit_info_metadata($asset_tags, $asset_id, $action = 'append', $user_id = '') {
    // The default action is to 'append'.
    $options = array();
    switch ($action) {
      case 'append':
      case 'update':
        $options = array('action' => $action);
        break;

      case 'replace':
        $options = array('replace' => 'TRUE');
        break;
    }

    // If user is provided, then overwrite session user_id.
    if (valid_email_address($user_id)) {
      $options += array(
        'user_id' => $user_id,
      );
    }

    // Get the metadata def.
    if (is_array($asset_tags)) {
      $options += $asset_tags;
    }
    else {
      $options += $this->build_metadata_array($asset_tags);
      if (empty($options)) {
        return FALSE;
      }
    }

    // Do request.
    if (!self::update_metadata($asset_id, $options)) {
      mediamosa_sb::watchdog_error('Unable to save metadata for asset %asset_id: %options', array(
        '%asset_id' => $asset_id,
        '%options' => print_r($options, TRUE),
      ));
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Creates an options array of DC and QDC metadata.
   *
   * This constructed array will need to be passed as argument
   * to the metadata submission function [submit_info()].
   *
   * @param array $values
   *   An array of metadata field names.
   *
   * @return array
   *   An array of (name, values) for all DC and QDC metadata.
   */
  public function metadata_to_options($values) {
    // Get the known metadata defitions (DC and QDC).
    $metadata_fields = mediamosa_ck::get_metadata_fields();
    // Construct the options array.
    foreach ($metadata_fields as $prefix => $metadata_field) {
      foreach ($metadata_field['fields'] as $name => $type) {
        // Build name.
        $prefixed_name = $prefix . '_' . $name;

        if (!isset($values[$prefixed_name]) || $values[$prefixed_name] === '') {
          continue;
        }

        // Set the array.
        $options[$name] = $values[$prefixed_name];
      }
    }
    return $options;
  }

  /**
   * Compares the asset tags with the application tags.
   *
   * @param string,array $asset_tags
   *   The tags for the current asset. Could be comma-separated string or array.
   *
   * @return array
   *   An array with the asset tags that are not included in the client app
   *   tags. Makes a difference to find the new tags.
   */
  public function compare($asset_tags) {
    $tags_diff = array();
    // If the tags are given in a comma separated string, then convert to array.
    if (!is_array($asset_tags)) {
      // First sanitize and encode the string.
      $asset_tags = explode(',', $this->encode(check_plain($asset_tags)));
    }
    // Obtain the new tags.
    $tags_diff = array_diff($asset_tags, $this->tags);
    return $tags_diff;
  }

  /**
   * Converts blank spaces to underscores.
   *
   * @param string,array $tags
   *   The tags that will be encoded (underscores will replace blank spaces).
   *
   * @return string
   *   A string of encoded tags.
   */
  public function encode($tags) {
    if (is_array($tags)) {
      $tags = implode(',', $tags);
    }
    $tags = explode(',', preg_replace('/\s+/', ' ', $tags));
    $tags_trimmed = array_map('trim', $tags);
    return str_replace(' ', '_', implode(',', $tags_trimmed));
  }

  /**
   * Converts underscores to blank spaces.
   *
   * @param string,array $tags
   *   The tags that will be decoded (underscores for blank spaces).
   * @param string $format
   *   The output format: Either 'string' (default) or 'array'.
   *
   * @return string,array
   *   A string or array of decoded tags, according to 'format' parameter.
   */
  public static function decode($tags, $format = 'string') {
    if (is_array($tags)) {
      $tags = implode(',', $tags);
    }
    $existent = array('_', ',');
    $replace  = array(' ', ', ');
    switch ($format) {
      case 'array':
        $string_tags = str_replace($existent, $replace, $tags);
        return explode(',', $string_tags);
        break;

      case 'link':
        $link_tags = array();
        $decoded_tags = explode(',', str_replace('_', ' ', $tags));
        $encoded_tags = explode(',', self::encode($tags));
        foreach ($decoded_tags as $key => $dtags) {
          $link_tags[] = l(trim($dtags), 'asset/tags/' . $encoded_tags[$key], array('attributes' => array('class' => 'mediadrive-free-tags')));
        }
        return implode(', ', $link_tags);
        break;

      case 'string':
      default:
        return str_replace($existent, $replace, $tags);
        break;
    }
  }

  /**
   * Builds a metadata array to be assigned to assets.
   *
   * @param string,array $asset_tags
   *   The tags for the current asset to be converted to array of tags ready to
   *   be passed as argument to the Media Drive API call.
   *
   * @return array
   *   An array of (tag name, value) for the current asset.
   */
  public function build_metadata_array($asset_tags) {
    $metadata_array = array();
    // If the tags are given in a comma separated string, then convert to array.
    if (!is_array($asset_tags)) {
      // First sanitize and encode the string.
      $asset_tags = explode(',', $this->encode(check_plain($asset_tags)));
    }
    foreach ($asset_tags as $asset_tag) {
      if (!empty($asset_tag)) {
        $metadata_array[$asset_tag] = $this->decode($asset_tag);
      }
    }
    return $metadata_array;
  }

  /**
   * Validates metadata_tags by checking it uses valid characters.
   *
   * Only allowed characters: A-Z, a-z, 0-9, blank space and comma.
   *
   * @param string $tags_text
   *   The comma-separated string of tags for the current asset.
   *
   * @return boolean
   *   TRUE if passes validation, FALSE otherwise.
   */
  public function validate($tags_text) {
    return !preg_match('/[^A-Za-z0-9,\ ]/', $tags_text);
  }

}
