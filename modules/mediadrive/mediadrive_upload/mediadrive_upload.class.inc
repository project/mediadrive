<?php

/**
 * @file Media Drive Upload
 * Wrapper class for upload functionality.
 */

class MediaDriveUpload {
  private $file = NULL;
  private $metadata = array();
  private $permissions = array();
  private $transcode_options = array();
  private $default_profile = NULL;
  private $upload_ticket = NULL;
  private $user_id;
  static public $image_allowed_extensions = array(
    'jpg', 'jpeg', 'gif', 'png',
  );
  static public $video_allowed_extensions = array(
    'asf', 'avi', 'mpeg', 'mpg', 'wmv','vob',
    'mov', 'mp4', 'flv', 'ogv', 'webm',
  );

  /**
   * Contructor method.
   *
   * @param $file The file to upload. Required.
   * @param $metadata Associative array of file's metadata. Has defaults.
   * @param $permissions Associative array of file's permissions. Has defaults.
   * @param $transcode_options Associative array of file's transcode options. Has defaults.
   *
   * @todo use overloaded arguments to avoid having to respect the order.
   */
  public function __construct($file, $metadata = array(), $permissions = array(), $transcode_options = array()) {
    $this->file = $file;
    $this->addMetadata($metadata);
    $this->user_id = mediamosa_ck::session_user_id();
    // Construct object with some default permissions and transcode options.
    $this->setPermissions(array_merge(array(
      'is_visible' => 1,
      'acl_domain' => '',
      'acl_realm' => '',
      'acl_user' => '',
    ), $permissions));
    // Depending on the file type, it might need some transcoding.
    switch ($file->type) {
      case 'image':
        break;

      case 'video':
      default:
        $this->getDefaultTranscodeProfile();
        $this->setTranscodeOptions(array_merge(array('active_' . $this->default_profile => 1, 'still_start_time' => '00:10'), $transcode_options));
        break;
    }
  }

// Public methods

  /**
   * Public logic control method.
   *
   */
  public function send($ticket_options = array()) {
    try {
      $this->createUploadTicket($ticket_options);
      $this->uploadFile();
      return array(
        'status' => TRUE,
        'asset_id' => $this->upload_ticket['asset_id'],
        'asset_url' => $this->getAssetDetailUrl(),
      );
    }
    catch (Exception $e) {
      return array(
        'status' => FALSE,
        'message' => $e->getMessage(),
      );
    }
  }

  /**
   * Static method to get the current allowed extensions to upload.
   *
   * Use this to get the file extensions on validation functions, before
   * Instantiating the class.
   *
   * @param string $type
   *   The file type. For example: 'image', 'video', 'all'.
   */
  static public function getAllowedExtensions($type = 'all') {
    switch ($type) {
      case 'image':
        return self::$image_allowed_extensions;
        break;

      case 'video':
        return self::$video_allowed_extensions;
        break;

      case 'all':
      default:
        return array_merge(self::$video_allowed_extensions, self::$image_allowed_extensions);
        break;
    }
  }

// Private methods

  /**
   * Public method to add metadata to this upload object.
   * Can be called as many times as necessary to add or replace the metadata
   * that will be sent with the file.
   *
   * @param $metadata Associative array with the metadata to update.
   *
   */
  public function addMetadata($metadata) {
    //@todo validate metadata keys against the API.
    if(!is_array($metadata)) {
      throw new Exception('Metadata needs to be an array');
    }
    try {
      return array(
        'status' => TRUE,
        'message' => $this->metadata = array_merge($this->metadata, $metadata),
      );
    }
    catch (Exception $e) {
      return array(
        'status' => FALSE,
        'message' => $e->getMessage(),
      );
    }
  }

  /**
   * Public method to set permissions associated with the file to be uploaded.
   * Can be called as many times as necessary to add or replace the permissions
   * that will be sent with the file.
   *
   * @param $permissions Associative array with the permissions to update.
   *
   */
  public function setPermissions($permissions) {
    //@todo validate permissions keys against the API.
    if(!is_array($permissions)) {
      throw new Exception('Permissions needs to be an array');
    }
    try {
      return array(
        'status' => TRUE,
        'message' => $this->permissions = array_merge($this->permissions, $permissions),
      );
    }
    catch (Exception $e) {
      return array(
        'status' => FALSE,
        'message' => $e->getMessage(),
      );
    }
  }

  /**
   * Public method to set transcode options associated with the file to be uploaded.
   * Can be called as many times as necessary to add or replace the options
   * that will be sent with the file.
   *
   * @param $transcode_options Associative array with the transcode options to update.
   *
   */
  public function setTranscodeOptions($transcode_options) {
    //@todo validate transcode options keys against the API.
    if(!is_array($transcode_options)) {
      throw new Exception('Transcode options needs to be an array');
    }
    try {
      return array(
        'status' => TRUE,
        'message' => $this->transcode_options = array_merge($this->transcode_options, $transcode_options),
      );
    }
    catch (Exception $e) {
      return array(
        'status' => FALSE,
        'message' => $e->getMessage(),
      );
    }
  }

  /**
   * Private method to get the default transcode profile.
   *
   */
  private function getDefaultTranscodeProfile() {
    if (!$default_profile = MediaMosaSbConnectorWrapper::get_default_transcode_profile()) {
      throw new Exception('Could not get default transcode profile');
    }
    else {
      return $this->default_profile = $default_profile;
    }
  }

  /**
   * Private method to get an upload ticket from the server.
   *
   */
  private function createUploadTicket($options) {
    $transcode_options = array(
      'create_still' => FALSE,
    );
    // Transcode depending on the file type.
    switch ($this->file->type) {
      case 'image':
        break;

      case 'video':
      default:
        $transcode_options['transcode'] = $this->default_profile ? array($this->default_profile) : array();
        break;
    }
    $options = array_merge($transcode_options, $options);
    if (!$upload_ticket = MediaMosaSbConnectorWrapper::create_upload_ticket($options)) {
      throw new Exception('Unable to create upload ticket.');
    }
    else {
      return $this->upload_ticket = $upload_ticket;
    }
  }

  /**
   * Private method to delete the asset created by the upload ticket.
   */
  private function destroy() {
    if (!MediaMosaSbConnectorWrapper::delete_asset($this->upload_ticket['asset_id'], $this->user_id)) {
      throw new Exception('Unable to delete asset ' . $this->upload_ticket['asset_id']);
    }
    else {
      return TRUE;
    }
  }

  /**
   * Private method to get the created asset local url.
   *
   */
  private function getAssetDetailUrl() {
    if (!$asset_url = mediamosa_sb::get_asset_detail_url($this->upload_ticket['asset_id'])) {
      throw new Exception('Unable to get asset url');
    }
    else {
      return $asset_url;
    }
  }

  /**
   * Private method to perform the upload of the file and set metadata, permissions
   * and transcode profiles.
   *
   */
  private function uploadFile() {
    $this->upload_ticket['action'] = $this->upload_ticket['action'] . '&filename=' . urlencode($this->file->filename);
    if (!$fh = fopen($this->file->uri, 'rb')) {
      throw new Exception('Could not open file');
    }
    if (!$ch = curl_init()) {
      throw new Exception('Could initialize cURL');
    }
    curl_setopt($ch, CURLOPT_URL, $this->upload_ticket['action']);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array());
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PUT, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 86400);
    curl_setopt($ch, CURLOPT_INFILE, $fh);
    curl_setopt($ch, CURLOPT_INFILESIZE, filesize($this->file->uri));

    if (!$data = curl_exec($ch)) {
      // There was a problem uploading the file, then delete the created asset.
      $this->destroy();
      throw new Exception('Could not send file');
    }
    try {
      // Post metadata.
      mediamosa_sb::submit_info($this->metadata, $this->upload_ticket['asset_id']);
      // Post permissions.
      mediamosa_sb::submit_access($this->permissions, $this->upload_ticket['asset_id']);
      // Post transcode options.
      switch ($this->file->type) {
        case 'image':
          break;

        case 'video':
        default:
          mediamosa_sb::submit_media($this->transcode_options, $this->upload_ticket['asset_id'], TRUE);
          break;
      }
    }
    catch(Exception $e) {
      throw new Exception ('Could not complete submission of uploaded file.');
    }
    return TRUE;
  }
}
