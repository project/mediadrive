<?php

/**
 * @file
 * Media Drive Still Upload
 * Wrapper class for Still upload functionality.
 */

class MediaDriveStillUpload {
  protected $mediafile_id = NULL;
  protected $asset_id = NULL;
  protected $file = NULL;
  protected $user_id = NULL;
  protected $ticket_id = NULL;
  protected $action_url = NULL;
  protected $options = array();

  /**
   * Contructor method.
   *
   * @param int $mediafile_id
   *   The Media file ID of the video to which we will upload a still.
   * @param int $asset_id
   *   The Asset ID of the video to which we will upload a still.
   * @param object $file
   *   The file to upload. Required.
   * @param array $options
   *   Associative array of still upload options.
   * @param string $user_id
   *   String that contains the email of the authorized user.
   *
   * @todo use overloaded arguments to avoid having to respect the order.
   */
  public function __construct($mediafile_id, $asset_id, $file, $options = array(), $user_id = NULL) {
    $this->mediafile_id = $mediafile_id;
    $this->asset_id = $asset_id;
    $this->file = $file;
    $this->addOptions($options);

    // If no user is given, it defaults to the user of the current session.
    $this->user_id = (!empty($user_id)) ? $user_id : mediamosa_ck::session_user_id();
  }

  /**
   * Public logic control method.
   *
   */
  public function send() {
    try {
      $this->createUploadTicket();
      $data = $this->uploadFile();
      return array(
        'status' => TRUE,
        'data' => $data,
      );
    }
    catch (Exception $e) {
      return array(
        'status' => FALSE,
        'data' => $data,
        'message' => $e->getMessage(),
      );
    }
  }

  /**
   * Public method to add options to this upload object.
   *
   * Can be called as many times as necessary to add or replace the metadata
   * that will be sent with the file.
   *
   * @param array $options
   *   Associative array with the options to update.
   */
  public function addOptions($options) {
    //@todo validate options keys against the API.
    if (!is_array($options)) {
      throw new Exception('Options needs to be an array');
    }
    return array(
      'status' => TRUE,
      'message' => $this->options = array_merge($this->options, $options),
    );
  }

// Private methods.

  /**
   * Private method to get an upload ticket from the server.
   */
  private function createUploadTicket() {
    $options = array(
      'user_id' => $this->user_id,
      'mediafile_id' => $this->mediafile_id,
    );

    $response = mediamosa_ck::request_post_fatal('mediafile/' . $this->mediafile_id . '/uploadticket/create', $options);
    if (!$response) {
      throw new Exception('Unable to create upload ticket');
    }
    else {
      $this->ticket_id = (string) $response->xml->items->item->ticket_id;
      $query = array(
        'still_upload' => 'TRUE',
        'filename' => urlencode($this->file->filename),
      );
      if (!empty($this->options['default']) && $this->options['default']) {
        $query['default'] = 'TRUE';
        unset($this->options['default']);
      }
      $this->action_url = url((string) $response->xml->items->item->action, array('query' => $query));
    }
  }

  /**
   * Private method to perform the upload of the file and set metadata,
   * permissions and transcode profiles.
   *
   */
  private function uploadFile() {
    // Obtaining upload options.
    $options = array_merge(array(
      'asset_id' => $this->asset_id,
      'mediafile_id' => $this->mediafile_id,
      'filename' => $this->file->filename,
      'file' => '@' . $this->file->real_filepath,
      // 'file' => '@' . $this->file->uri,
    ), $this->options);

    if (!$ch = curl_init()) {
      throw new Exception('Could not initialize cURL');
    }
    curl_setopt($ch, CURLOPT_URL, $this->action_url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array());
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $options);
    curl_setopt($ch, CURLOPT_TIMEOUT, 86400);
    curl_setopt($ch, CURLOPT_INFILESIZE, filesize($this->file->uri));
    if (!$data = curl_exec($ch)) {
      throw new Exception('Could not send file');
    }
    return $data;
  }
}
