(function($) {
  // Behavior to make the upload field awesome.
  Drupal.behaviors.mediadriveUploadFormUploadField = {
    attach: function (context) {

      // Determine the allowed file types.
      function determine_file_type(filename) {
        var extension = filename.split('.').pop();
        var is_allowed_image = $.inArray(extension, Drupal.settings.mediadriveUploadForm.image_allowed_extensions);
        var is_allowed_video = $.inArray(extension, Drupal.settings.mediadriveUploadForm.video_allowed_extensions);
        if (is_allowed_image != -1) {
          return 'image';
        }
        else if (is_allowed_video != -1) {
          return 'video';
        } else {
          return 'not allowed';
        }
      }
      // Defining all the different handlers for file types.
      var mediadrive_file_handlers = {
        'image': function image_callback() {
                   $('.form-item-still-start-time').hide();
                   $('#edit-transcode-options').hide();
                   $('.mediadrive-upload-wrapper .form-text').removeClass('error');
                   $('#file-not-allowed').remove();
                   $('.mediadrive-button-to-block').removeClass('pressed');
                 },
        'video': function video_callback() {
                   $('.form-item-still-start-time').show();
                   $('#edit-transcode-options').show();
                   $('.mediadrive-upload-wrapper .form-text').removeClass('error');
                   $('#file-not-allowed').remove();
                   $('.mediadrive-button-to-block').removeClass('pressed');
                 },
        'not allowed': function not_allowed_callback() {
                   $('.mediadrive-button-to-block').addClass('pressed');
                   $('.mediadrive-upload-wrapper .form-text').addClass('error');
                   $('#file-not-allowed').remove();
                   $('.form-item-files-file').append('<div id="file-not-allowed" class="description messages error"><ul><li>File not allowed. Please select again.</li><ul></div>').fadeIn('slow');
                 }
      }
      // Processing selected file.
      $('.mediadrive-upload-filefield').wrap('<div class="mediadrive-upload-wrapper" />');
      $('.mediadrive-upload-wrapper').css({
        "position" : "relative"
      });
      var width = $('.mediadrive-upload-filefield').width();
      $('.mediadrive-upload-filefield').css({
        "opacity" : "0",
        "margin" : "0",
        "padding" : "0"
      });
      $('.mediadrive-upload-wrapper').append('<input class="form-text mediadrive-upload-fakefile" type="text" maxlength="128" size="60" value=""><input class="form-submit mediadrive-upload-fakefile" type="submit" value="Browse" name="op">');
      $('.mediadrive-upload-wrapper .form-text').css({
        "position" : "absolute",
        "top" : "0",
        "left" : "0",
        "width" : width
      });
      $('.form-submit.mediadrive-upload-fakefile').css({
        "margin-left" : "0.85em"
      });
      $('.mediadrive-upload-fakefile').click(function(e){
        $('.mediadrive-upload-filefield').click();
        return false;
      });
      $('.mediadrive-upload-filefield').change(function(e){
        $('.mediadrive-upload-wrapper .form-text').val($(this).val());
        if ($('.mediadrive-fieldset-basics').hasClass('collapsed')) {
          $('.mediadrive-fieldset-basics .fieldset-title').click();
        }
        var filename = $(this).val();
        $('.mediadrive-title').val(filename).select();
        // Determine and call appropiate file type handlers:
        var type = determine_file_type(filename);
        mediadrive_file_handlers[type]();
      });
    }
  };
  // Behavior to disable the default transcode profile.
  Drupal.behaviors.mediadriveUploadFormDefaultTranscodeProfile = {
    attach: function (context) {
      var default_checkbox = $('.mediadrive-transcode-options').find('input[value="' + Drupal.settings.mediadriveUploadForm.default_transcode_profile + '"]');
      default_checkbox.next('label').text('(Default) ' + default_checkbox.next('label').text());
      default_checkbox.click(function(e){
        e.preventDefault();
      });
    }
  };
  // Behavior to handle multiple clicks on form submission button.
  Drupal.behaviors.mediadriveButtonToBlock = {
    attach: function (context) {
      var button = $('.mediadrive-button-to-block');
      button.click(function(e){
        if ($(this).hasClass('pressed')) {
          return false;
        }
        else {
          $(this).addClass('pressed');
          $('#mediadrive-upload-form').submit();
        }
      });
    }
  };

})(jQuery);


