(function($) {
  // Behavior to fix advanced search.
  Drupal.behaviors.mediadriveAdvancedSearchForm = {
    attach: function (context) {
      var advanced_search = $('fieldset.search-advanced');
      // Initialy hide all items that aren't controllers.
      advanced_search.find('.restriction-wrapper').each(function(e){
        var id = $(this).attr('id');
        if(!$(this).find('.restriction-controller').is(':checked')){
          $(this).children('.form-item').not('.form-item-use-' + id).hide();
        }
      });
      advanced_search.find('.restriction-controller').change(function(e){
        if($(this).is(':checked')) {
          $(this).parent().siblings().show();
        }
        else {
          $(this).parent().siblings().hide();
        }
      });
    }
  };
})(jQuery);
