<?php
/**
 * @file
 * mediadrive_multitenant_settings.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function mediadrive_multitenant_settings_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_mediamosa_connector|node|subscription|form';
  $field_group->group_name = 'group_mediamosa_connector';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'subscription';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Acquia Media Drive connection settings',
    'weight' => '2',
    'children' => array(
      0 => 'field_mediamosa_connector_url',
      1 => 'field_mediamosa_connector_user',
      2 => 'field_mediamosa_connector_pass',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_mediamosa_connector|node|subscription|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings_still|node|subscription_settings|form';
  $field_group->group_name = 'group_settings_still';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'subscription_settings';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Still generate parameters',
    'weight' => '2',
    'children' => array(
      0 => 'field_settings_still_type',
      1 => 'field_settings_still_size',
      2 => 'field_settings_still_h_padding',
      3 => 'field_settings_still_v_padding',
      4 => 'field_settings_still_time_code',
      5 => 'field_settings_still_per_file',
      6 => 'field_settings_still_every_sec',
      7 => 'field_settings_still_start_time',
      8 => 'field_settings_still_end_time',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_settings_still|node|subscription_settings|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings_transcode|node|subscription_settings|form';
  $field_group->group_name = 'group_settings_transcode';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'subscription_settings';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Transcode profile parameter',
    'weight' => '3',
    'children' => array(
      0 => 'field_settings_transcode_default',
      1 => 'field_settings_transcode_allowed',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_settings_transcode|node|subscription_settings|form'] = $field_group;

  return $export;
}
