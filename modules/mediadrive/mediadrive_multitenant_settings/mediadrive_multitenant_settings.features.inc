<?php
/**
 * @file
 * mediadrive_multitenant_settings.features.inc
 */

/**
 * Implements hook_node_info().
 */
function mediadrive_multitenant_settings_node_info() {
  $items = array(
    'subscription' => array(
      'name' => t('Subscription'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
    'subscription_settings' => array(
      'name' => t('Subscription settings'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Subscription settings name'),
      'help' => '',
    ),
    'user_reference' => array(
      'name' => t('User reference'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('User reference name'),
      'help' => '',
    ),
  );
  return $items;
}
