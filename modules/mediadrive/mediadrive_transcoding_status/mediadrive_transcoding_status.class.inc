<?php

/**
 * @file
 * MediaCloud transcoding status related functions.
 */
class MediaDriveTranscodingStatus extends MediaMosaCkConnectorTranscodeWrapper {

  /**
   * Retrieve all outstanding jobs.
   *
   * @param string $user_id
   *
   * Array with jobs.
   */
  static public function get_all_outstanding_jobs($user_id) {

    // REST uri.
    $uri = strtr('user/@user_id/joblist', array('@user_id' => $user_id));

    try {
      $result = mediamosa_ck::request_get_fatal($uri, array('user_id' => $user_id));
    } catch (Exception $e) {
      mediamosa_ck::watchdog_error('Unable to retrieve jobs; @message.', array('@message' => $e->getMessage()));
      return FALSE;
    }

    return $result;
  }

  /**
   * Retrieve all outstanding jobs for a given asset.
   *
   * @param string $asset_id
   * @param string $user_id
   *
   * Array with jobs.
   */
  static public function get_all_outstanding_jobs_per_asset($asset_id, $user_id) {

    // REST uri.
    $uri = strtr('asset/@asset_id/joblist', array('@asset_id' => $asset_id));

    try {
      $result = mediamosa_ck::request_get_fatal($uri, array('user_id' => $user_id));
    } catch (Exception $e) {
      mediamosa_ck::watchdog_error('Unable to retrieve jobs; @message.', array('@message' => $e->getMessage()));
      return FALSE;
    }

    return $result;
  }

}
