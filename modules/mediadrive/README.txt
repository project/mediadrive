Media Drive modules are meant to extend the site builder and sdk functionality,
without altering their code.
Use this directory to place modules meant to be shared with clients.
