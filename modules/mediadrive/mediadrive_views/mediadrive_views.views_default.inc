<?php
/**
 * @file
 * mediadrive_views.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function mediadrive_views_views_default_views() {
  $export = array();

  // Defines a default view for metadata_tags.
  $view = new view();
  $view->name = 'mediadrive_asset_tags';
  $view->description = 'Show a listing of assets given a free-tag';
  $view->tag = 'default';
  $view->base_table = 'mediadrive_assets_in_freetag';
  $view->human_name = 'MEDIADRIVE Tags';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Tags';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '12';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Acquia Media Drive: Asset ID */
  $handler->display->display_options['fields']['asset_id']['id'] = 'asset_id';
  $handler->display->display_options['fields']['asset_id']['table'] = 'mediadrive_assets_in_freetag';
  $handler->display->display_options['fields']['asset_id']['field'] = 'asset_id';
  /* Field: Acquia Media Drive: Created */
  $handler->display->display_options['fields']['videotimestamp']['id'] = 'videotimestamp';
  $handler->display->display_options['fields']['videotimestamp']['table'] = 'mediadrive_assets_in_freetag';
  $handler->display->display_options['fields']['videotimestamp']['field'] = 'videotimestamp';
  $handler->display->display_options['fields']['videotimestamp']['label'] = '';
  $handler->display->display_options['fields']['videotimestamp']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['videotimestamp']['date_format'] = 'mediamosa_ck_normal';
  /* Field: Acquia Media Drive: Granted */
  $handler->display->display_options['fields']['granted']['id'] = 'granted';
  $handler->display->display_options['fields']['granted']['table'] = 'mediadrive_assets_in_freetag';
  $handler->display->display_options['fields']['granted']['field'] = 'granted';
  $handler->display->display_options['fields']['granted']['label'] = '';
  $handler->display->display_options['fields']['granted']['element_label_colon'] = FALSE;
  /* Field: Acquia Media Drive: Mediafile duration */
  $handler->display->display_options['fields']['mediafile_duration']['id'] = 'mediafile_duration';
  $handler->display->display_options['fields']['mediafile_duration']['table'] = 'mediadrive_assets_in_freetag';
  $handler->display->display_options['fields']['mediafile_duration']['field'] = 'mediafile_duration';
  $handler->display->display_options['fields']['mediafile_duration']['label'] = '';
  $handler->display->display_options['fields']['mediafile_duration']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['mediafile_duration']['date_format'] = 'mediamosa_ck_normal';
  /* Field: Acquia Media Drive: Owner */
  $handler->display->display_options['fields']['owner_id']['id'] = 'owner_id';
  $handler->display->display_options['fields']['owner_id']['table'] = 'mediadrive_assets_in_freetag';
  $handler->display->display_options['fields']['owner_id']['field'] = 'owner_id';
  $handler->display->display_options['fields']['owner_id']['label'] = '';
  $handler->display->display_options['fields']['owner_id']['element_label_colon'] = FALSE;
  /* Field: Acquia Media Drive: Played */
  $handler->display->display_options['fields']['played']['id'] = 'played';
  $handler->display->display_options['fields']['played']['table'] = 'mediadrive_assets_in_freetag';
  $handler->display->display_options['fields']['played']['field'] = 'played';
  $handler->display->display_options['fields']['played']['label'] = '';
  $handler->display->display_options['fields']['played']['element_label_colon'] = FALSE;
  /* Field: Acquia Media Drive: Still */
  $handler->display->display_options['fields']['still_url']['id'] = 'still_url';
  $handler->display->display_options['fields']['still_url']['table'] = 'mediadrive_assets_in_freetag';
  $handler->display->display_options['fields']['still_url']['field'] = 'still_url';
  $handler->display->display_options['fields']['still_url']['label'] = '';
  $handler->display->display_options['fields']['still_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['still_url']['still_style'] = 'mediamosa_sb_large';
  /* Field: Acquia Media Drive: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'mediadrive_assets_in_freetag';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Contextual filter: Acquia Media Drive: The free tag */
  $handler->display->display_options['arguments']['tag']['id'] = 'tag';
  $handler->display->display_options['arguments']['tag']['table'] = 'mediadrive_assets_in_freetag';
  $handler->display->display_options['arguments']['tag']['field'] = 'tag';
  $handler->display->display_options['arguments']['tag']['default_action'] = 'default';
  $handler->display->display_options['arguments']['tag']['default_argument_type'] = 'raw';
  $handler->display->display_options['arguments']['tag']['default_argument_options']['index'] = '2';
  $handler->display->display_options['arguments']['tag']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tag']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tag']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['tag']['limit'] = '0';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'asset/tags';

  $export['mediadrive_asset_tags'] = $view;
  return $export;
}
