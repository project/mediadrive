<?php
/**
 * MEDIAMOSA is Open Source Software to build a Full Featured, Webservice
 * Oriented Media Management and Distribution platform (http://mediamosa.org)
 *
 * Copyright (C) 2012 SURFnet BV (http://www.surfnet.nl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, you can find it at:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

class mediadrive_views_field_acl_info extends views_handler_field {
  // The following methods express strong SQLisms in the base field handler
  // class. Override and blank them out.
  function pre_render(&$values) {
  }

  function add_additional_fields($fields = NULL) {
    foreach ($fields as $field) {
      $this->aliases[$field] = $this->query->add_field($this->table, $field);
    }
  }

  function click_sort($order) {
    $this->query->add_orderby($this->table, $this->real_field, $order);
  }

  function query() {
    $this->field_alias = $this->query->add_field($this->table, $this->real_field);

    // Add in additional fields.
    if (!empty($this->additional_fields)) {
      $this->add_additional_fields($this->additional_fields);
    }
  }

  /**
   * Setup default for options.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['acl_domain_label'] = array('default' => 'Domain');
    $options['acl_realm_label'] = array('default' => 'Realm');
    $options['acl_user_label'] = array('default' => 'Email');

    return $options;
  }

  /**
   * Still options.
   */
  function options_form(&$form, &$form_state) {

    $form['acl_domain_label'] = array(
      '#title' => t('Domain acl label'),
      '#type' => 'textfield',
      '#default_value' => $this->options['acl_domain_label'],
    );
    $form['acl_realm_label'] = array(
      '#title' => t('Realm acl label'),
      '#type' => 'textfield',
      '#default_value' => $this->options['acl_realm_label'],
    );
    $form['acl_user_label'] = array(
      '#title' => t('User acl label'),
      '#type' => 'textfield',
      '#default_value' => $this->options['acl_user_label'],
    );
    parent::options_form($form, $form_state);
  }

  function render($values) {
    global $user;

    // To render, asset_id must be present.
    if (empty($values->asset_id)) {
      return;
    }

    // Empty at start.
    $options = array();

    // What mediafile to play?
    $mediafile_id = empty($values->mediafile_id_to_play) ? NULL : $values->mediafile_id_to_play;

    // Take user_id of current watcher.
    $user_id = mediamosa_sb::get_owner_asset($values->asset_id);

    if (!$mediafile_id) {
      $mediafile_ids = MediaMosaSbConnectorWrapper::get_asset_mediafile_ids($values->asset_id, array('get_original_only' => TRUE));
      $mediafile_id = reset($mediafile_ids);
    }

    try {
      $access = MediaMosaSbConnectorWrapper::get_access_control($mediafile_id, $user_id);
      $response = theme('mediadrive_views_theme_acl_info',
        array(
          'acl_domain' => array('label' => $this->options['acl_domain_label'], 'value' => implode(', ', $access['acl_domain'])),
          'acl_realm' => array('label' => $this->options['acl_realm_label'], 'value' => implode(', ', $access['acl_realm'])),
          'acl_user' => array('label' => $this->options['acl_user_label'], 'value' => implode(', ', $access['acl_user'])),
        ));
    }
    catch (Exception $e) {
      $response = t('Cannot display permissions information for this asset.');
    }
    return $response;
  }
}
