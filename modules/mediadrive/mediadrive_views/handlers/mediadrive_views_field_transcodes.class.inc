<?php
/**
 * MEDIAMOSA is Open Source Software to build a Full Featured, Webservice
 * Oriented Media Management and Distribution platform (http://mediamosa.org)
 *
 * Copyright (C) 2012 SURFnet BV (http://www.surfnet.nl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, you can find it at:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

class mediadrive_views_field_transcodes extends views_handler_field {
  // The following methods express strong SQLisms in the base field handler
  // class. Override and blank them out.
  function pre_render(&$values) {
  }

  function add_additional_fields($fields = NULL) {
    foreach ($fields as $field) {
      $this->aliases[$field] = $this->query->add_field($this->table, $field);
    }
  }

  function click_sort($order) {
    $this->query->add_orderby($this->table, $this->real_field, $order);
  }

  function query() {
    $this->field_alias = $this->query->add_field($this->table, $this->real_field);

    // Add in additional fields.
    if (!empty($this->additional_fields)) {
      $this->add_additional_fields($this->additional_fields);
    }
  }

  function render($values) {
    global $user;

    // To render, asset_id must be present.
    if (empty($values->asset_id)) {
      return;
    }

    // What mediafile to play?
    $mediafile_id_to_play = empty($values->mediafile_id_to_play) ? NULL : $values->mediafile_id_to_play;

    // Take user_id of current watcher.
    $user_id = mediamosa_ck::session_user_id();

    // Determine admin user
    if (user_access('access administration pages')) {
      $options['is_app_admin'] = TRUE;
    }
    $transcodes = array();
    try {
      $options['fatal'] = TRUE;
      $response = MediaDriveTranscodingStatus::get_all_outstanding_jobs_per_asset($values->asset_id, $user_id);
      foreach($response->xml->items->item as $transcode) {
        $transcodes[] = array(
          'id' => (int) $transcode->id,
          'progress' => (string) $transcode->progress,
          'job_type' => (string) $transcode->job_type,
        );
      }

    }
    catch (Exception $e) {
      $transcodes = NULL;
    }
    return theme('mediadrive_views_theme_transcodes', array('data' => $transcodes));
  }
}

