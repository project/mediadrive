<?php
/**
 * MEDIAMOSA is Open Source Software to build a Full Featured, Webservice
 * Oriented Media Management and Distribution platform (http://mediamosa.org)
 *
 * Copyright (C) 2012 SURFnet BV (http://www.surfnet.nl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, you can find it at:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

class mediadrive_views_field_embed extends views_handler_field {
  // The following methods express strong SQLisms in the base field handler
  // class. Override and blank them out.
  function pre_render(&$values) {
  }

  function add_additional_fields($fields = NULL) {
    foreach ($fields as $field) {
      $this->aliases[$field] = $this->query->add_field($this->table, $field);
    }
  }

  function click_sort($order) {
    $this->query->add_orderby($this->table, $this->real_field, $order);
  }

  function query() {
    $this->field_alias = $this->query->add_field($this->table, $this->real_field);

    // Add in additional fields.
    if (!empty($this->additional_fields)) {
      $this->add_additional_fields($this->additional_fields);
    }
  }

  /**
   * Setup default for options.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['response'] = array('default' => 'object');
    $options['player_width'] = array('default' => '640');
    $options['player_height'] = array('default' => '360');
    $options['player_autostart'] = array('default' => 0);

    return $options;
  }

  /**
   * Still options.
   */
  function options_form(&$form, &$form_state) {

    $form['player_width'] = array(
      '#title' => t('Player width in embed code'),
      '#description' => t('Only for response HTML object code.'),
      '#type' => 'textfield',
      '#default_value' => $this->options['player_width'],
    );
    $form['player_height'] = array(
      '#title' => t('Player height in embed code'),
      '#description' => t('Only for response HTML object code.'),
      '#type' => 'textfield',
      '#default_value' => $this->options['player_height'],
    );
    parent::options_form($form, $form_state);
  }

  function render($values) {
    global $user;

    // To render, asset_id must be present.
    if (empty($values->asset_id)) {
      return;
    }

    // Empty at start.
    $options = array();

    // What mediafile to play?
    $mediafile_id_to_play = empty($values->mediafile_id_to_play) ? NULL : $values->mediafile_id_to_play;

    // Take user_id of current watcher.
    $user_id = mediamosa_ck::session_user_id();

    // Determine admin user
    if (user_access('access administration pages')) {
      $options['is_app_admin'] = TRUE;
    }

    // Determining the type of asset.
    $asset_type = reset(explode('/', $values->technical_metadata['mime_type']));

    // Response.
    $options['response'] = $this->options['response'];

    // Not specified? Then use default transcode profile ID.
    if (empty($mediafile_id_to_play)) {
      if ($asset_type == 'image') {
        $mediafile_id_to_play = $values->mediafile_id_original;
      }
      else {
        $options['profile_id'] = MediaMosaCkConnectorWrapper::get_default_transcode_profile();
      }
    }

    // Both not empty.
    if (!empty($this->options['player_width']) && !empty($this->options['player_height'])) {
      $options['width'] = $this->options['player_width'];
      $options['height'] = $this->options['player_height'];
    }

    try {
      $options['fatal'] = TRUE;
      $response = check_plain(MediaMosaCkConnectorWrapper::get_play_link($values->asset_id, $mediafile_id_to_play, $user_id, $options));
    }
    catch (Exception $e) {
      $response = t('Embed code not available');
    }
    return theme('mediadrive_views_theme_embed', array('code' => $response));
  }
}
