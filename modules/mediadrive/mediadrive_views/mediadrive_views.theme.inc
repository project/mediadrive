<?php
/**
 * @file
 * Theme related functions.
 */

/**
 * The theme will generate an embed code textarea field.
 *
 * @param type $variables
 */
function theme_mediadrive_views_theme_embed($variables) {
  return '<textarea readonly>' . $variables['code'] . '</textarea>';
}

/**
 * The theme will generate a current transcodes widget.
 *
 * @param type $variables
 */
function theme_mediadrive_views_theme_transcodes($variables) {
  $output = '';
  if(!$variables['data']) {
    return t('No transcode data available');
  }
  foreach($variables['data'] as $transcode) {
    $progress = $transcode['progress'] * 100;
    if ($progress >= 100) {
      $legend = t('completed');
      $status = 'complete';
    }
    else {
      $legend = $progress . '%';
      $status = 'incomplete';
    }
    $output .= '<div class="asset-transcode ' . strtolower($transcode['job_type']) . '">';
    $output .= '<div class="asset-transcode-id">' . t('Job id @id', array('@id' => $transcode['id'])) . '</div>';
    $output .= '<div class="asset-transcode-progress-wrapper">';
    $output .= '<div class="asset-transcode-progress ' . $status . '" style="width:' . $progress . '%">';
    if ($progress > 25) {
      $output .= '<span class="asset-transcode-legend">' . $legend . '</span></div>';
    }
    else {
      $output .= '</div><span class="asset-transcode-legend">' . $legend . '</span>';
    }
    $output .= '</div>';
    $output .= '</div>';
  }
  return $output;
}

/**
 * The theme will generate markup for the acl information.
 *
 * @param type $variables
 */
function theme_mediadrive_views_theme_acl_info($variables) {
  $output = '';
  foreach($variables as $section) {
    $output .= '<div class="asset-subsection">';
    $output .= '<h3 class="asset-subsection-title">' . $section['label'] . '</h3>';
    $output .= '<p class="asset-subsection-value">' . (!isset($section['value']) ? t('not set') : $section['value']) . '</p>';
    $output .= '</div>';
  }
  return $output;
}
