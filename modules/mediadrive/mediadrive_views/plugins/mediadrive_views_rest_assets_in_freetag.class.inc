<?php
/**
 * Acquia Media Drive is Open Source Software to build a Full Featured, Webservice
 * Oriented Media Management and Distribution platform (http://mediamosa.org)
 *
 * Copyright (C) 2012 SURFnet BV (http://www.surfnet.nl)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, you can find it at:
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

/**
 * @file
 * The plugin query object for Acquia Media Drive allows REST calls inside views. It
 * uses the Views 3 plugin option to query another source instead of the normal
 * Drupal database.
 *
 * View will return the most popular assets for a specific tag.
 *
 */
class mediadrive_views_rest_assets_in_freetag extends mediamosa_ck_views_rest_asset_search {
  /**
   * Execute the REST call.
   *
   * @param $view
   *   The view object.
   * @param $params
   *   Array of view options and settings.
   * @param $options
   *   Options that can be used for overrides on the view params and REST call
   *   related options.
   */
  protected function do_rest_call($view, array $params, array $options = array()) {

    // If not set, use default for order;
    if (empty($this->orderby)) {
      $this->orderby[__CLASS__ . '.' . 'numofviews'] = array('orderby' => 'videotimestamp', 'order' => 'ASC', 'params' => array());
    }

    if (empty($view->args)) {
      return;
    }

    // Expect first argument to be the freetag ID.
    $freetag = reset($view->args);
    // $freetag_decoded = mediadrive_sdk_metadata_tags::decode($freetag_id);
    $freetag_decoded = str_replace('_', ' ', $freetag);
    $options['cql'] = $freetag . '=="^' . $freetag_decoded . '^"';

    // Call parent.
    parent::do_rest_call($view, $params, $options);
  }

  /**
   * Get the views data for this REST call.
   *
   * @return
   */
  static public function get_views_data($title, $class) {
    $data = array(
      'table' => array(
        'group' => t('Acquia Media Drive'),
        'base' => array(
          'field' => 'id',
          'title' => t($title),
          'help' => t('Retrieve and search assets in the Acquia Media Drive backend'),
          'query class' => $class,
        ),
      ),

      // Search fields.
      'keywords' => array(
        'title' => t('The search box'),
        'help' => t('Unique Identifier of each asset.'),
        'filter' => array(
          'handler' => 'mediamosa_ck_views_filter_text',
        ),
      ),

      'coll_id' => array(
        'title' => t('Collection ID'),
        'help' => t('The collection ID of the object.'),
        'filter' => array(
          'handler' => 'mediamosa_ck_views_filter_collections',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),

      'tag' => array(
        'title' => t('The free tag'),
        'help' => t('The free-tag name of the object.'),
        'filter' => array(
          'handler' => 'mediadrive_views_filter_tag',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),

    );

    // Add asset stuff.
    self::get_views_data_asset_common($data);

    // Owner / group.
    self::get_views_data_owner_stuff($data);

    // Add metadata.
    self::get_views_data_metadata($data);

    return $data;
  }

}
