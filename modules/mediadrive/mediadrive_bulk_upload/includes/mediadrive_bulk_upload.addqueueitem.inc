<?php
/**
 * @file
 * Provides a function for adding items to the queue.
 */

/**
 * Adds new item to the queue.
 *
 * This function will be executed through an ajax call to add a new
 * item in the queue to be processed later.
 */
function mediadrive_bulk_upload_add_queue_item() {
  global $user;
  // Verifying that the request is coming from ajax.
  $type = isset($_POST['type']) ? $_POST['type'] : '';
  $sub = isset($_POST['sub']) ? $_POST['sub'] : '';
  $mediadrive_token = isset($_POST['mediadrive_token']) ? $_POST['mediadrive_token'] : '';
  if ($type == 'ajax' && drupal_valid_token($mediadrive_token, 'pR0t3cT3Dm3DiA') && is_numeric($sub)) {
    // Obtaining module settings.
    $data = _mediadrive_bulk_upload_session_get('mediadrive_bulk_upload_params', array());
    // The 'id' variable contains the filename.
    $id = isset($_POST['id']) ? $_POST['id'] : '';
    $metadata = isset($_POST['metadata']) ? $_POST['metadata'] : '';
    $file_info = pathinfo($id);
    $data['id'] = $id;
    $data['user_id'] = isset($data['user_id']) ? $data['user_id'] : mediamosa_ck::session_user_id();
    $data['uid'] = $user->uid;
    $data['sub'] = $sub;
    $data = array_merge($data, $metadata);
    $data['metadata']['title'] = basename($id, '.' . $file_info['extension']);

    // Store values in the mediamosaBulkUpload queue.
    $queue = DrupalQueue::get('mediadrivebulkupload');
    $item_id = $queue->createItem($data);
    drupal_json_output($item_id);
    drupal_exit();
  }
  else {
    // This is supposed to run on js only.
    drupal_json_output(FALSE);
  }
}
