<?php
/**
 * @file
 * Provides an interface for the upload of Video files to Acquia Media Drive.
 */

/**
 * Implements the page for uploading video files to Acquia Media Drive.
 */
function mediadrive_bulk_upload_uploadpage() {
  // Reading the pre-defined settings.
  $mediadrive_bulk_upload_params = _mediadrive_bulk_upload_session_get('mediadrive_bulk_upload_params', array());
  // Check that the user has chosen a specific settings for video upload.
  if (empty($mediadrive_bulk_upload_params)) {
    drupal_set_message('You have to provide "bulk upload parameters" (transcoding profiles, time for still generation and collections) that will be used for bulk upload.');
    drupal_goto('asset/bulk_upload');
  }
  // Setting up a security token.
  $mediadrive_token = drupal_get_token('pR0t3cT3Dm3DiA');
  $mediadrive_bulk_upload = variable_get('mediadrive_bulk_upload', array(
    'video_path' => 'mediadrive_file_uploads',
    'maxfilesize' => '267386880',
  ));
  drupal_add_js(array(
      'mediadrive_token' => $mediadrive_token,
      'sub_id' => MEDIADRIVEMultitenant::getSubscriptionId(),
      'maxfilesize' => $mediadrive_bulk_upload['maxfilesize'],
      'files' => array(),
      'allowed_extensions' => implode('|', MediaDriveUpload::getAllowedExtensions()),
    ),
  'setting');

  // Adding libraries.
  drupal_add_library('mediadrive_bulk_upload', 'twitter_bootstrap');
  drupal_add_library('mediadrive_bulk_upload', 'jquery_file_upload');

  // Adding javascript.
  drupal_add_js(drupal_get_path('module', 'mediadrive_bulk_upload') . '/js/main.js', array('scope' => 'header'));

  // Get the Subscription ID.
  $sub_id = MEDIADRIVEMultitenant::getSubscriptionId();
  // Loading the template with translated strings.
  $contents = array(
    'notice' => t('Upload files with a maximum size of %maxfilesize.', array(
      '%maxfilesize' => number_format($mediadrive_bulk_upload['maxfilesize'] / (1024 * 1024)) . 'MB',
     )),
    'add_files' => t('Add files...'),
    'start_upload' => t('Start upload'),
    'cancel_upload' => t('Cancel'),
    'delete_files' => t('Delete'),
    'download' => t('Download'),
    'slideshow' => t('Slideshow'),
    'previous' => t('Previous'),
    'next' => t('Next'),
  );
  $output = theme('mediadrive_bulk_upload_form', array(
    'mediadrive_token' => $mediadrive_token,
    'sub_id' => $sub_id,
    'contents' => $contents,
  ));
  return $output;
}
