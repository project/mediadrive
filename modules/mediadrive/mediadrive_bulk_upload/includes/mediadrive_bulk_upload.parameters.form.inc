<?php

/**
 * Generates the list of checkboxes for transcoding profiles.
 *
 * @param $form
 *   The drupal form array.
 * @param $profiles
 *   The array of transcoding profiles.
 *
 * @return
 *   Returns the form array.
 */
function _mediadrive_bulk_upload_transcode_settings($form, $profiles = array()) {
  $form['transcode'] = array(
    '#type' => 'fieldset',
    '#title' => t('Currently active video profiles'),
  );

  $xml = MediaMosaSbConnectorWrapper::get_transcode_profiles();
  if ($xml == FALSE) {
    drupal_set_message(t('Unable to retrieve transcode profiles, check Acquia Media Drive Connector.'), 'error');
  }
  // Get the default profiles, these can not be deleted or re-created.
  $default_profile = MediaMosaSbConnectorWrapper::get_default_transcode_profile();

  // Get the allowed profiles.
  foreach (MediaMosaSbConnectorWrapper::get_transcode_profiles()->items->item as $profile) {
    if ($profile->default == 'TRUE') {
      $transcode_profiles['default'][(string) $profile->profile_id] = (string) $profile->profile_id;
    }
    $transcode_profiles['profiles'][(string) $profile->profile_id] = (string) $profile->profile;
  }

  if (!empty($default_profile) && !empty($transcode_profiles['profiles'][$default_profile])) {
    $transcode_profiles['profiles'][$default_profile] .= t(' (Default Transcoding Profile)');
  }

  // Build the list of transcoding profiles.
  $form['transcode']['profiles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Transcoding Profiles'),
    '#options' => $transcode_profiles['profiles'],
    '#default_value' => array($default_profile),
  );
  $form['transcode']['default_profile'] = array(
    '#type' => 'hidden',
    '#value' => $default_profile,
  );
  return $form;
}

/**
 * The still form.
 *
 * @param $form
 *   The drupal form array.
 * @param $value
 *   The value of the field.
 *
 * @return
 *   Returns the form array.
 */
function _mediadrive_bulk_upload_still_settings($form, $value = '') {
  $form['still'] = array(
    '#type' => 'fieldset',
    '#title' => t('Still generation for this video'),
  );

  $form['still']['still_start_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Time for still generation'),
    '#required' => FALSE,
    '#size' => 5,
    '#default_value' => isset($value) ? $value : mediamosa_sb_asset_forms::DEFAULT_START_TIME_STILL_FORM,
    '#description' => t('Enter the time in MM:SS (minutes:seconds) format for the frame on which the still wil be generated'),
  );

  return $form;
}

/**
 * The collections form.
 *
 * @param $form
 *   The drupal form array.
 * @param $value
 *   The value of the collections form elemeent.
 *
 * @return
 *   Returns the form array.
 */
function _mediadrive_bulk_upload_collections_settings($form, $value = '') {
  // Get the current user.
  $user_id = mediamosa_ck::session_user_id();
  // $user_id = MediaMosaSbConnectorWrapper::get_asset_owner($asset_id);
  // Get the collections.
  $options_collections = MediaMosaSbConnectorWrapper::get_collections_for_options();

  $form['collections'] = array(
    '#type' => 'fieldset',
    '#title' => t('Collections'),
  );

  if (count($options_collections)) {
    $form['collections']['mycollections'] = array(
      '#type' => 'select',
      '#title' => t('My collections'),
      '#required' => FALSE,
      '#multiple' => TRUE,
      '#size'=> 5,
      '#options' => $options_collections,
      '#default_value' => !isset($value) ? '' : $value,
    );
  }
  else {
    $form['collections']['mycollections'] = array(
      '#markup' => '<p><br />' . t('No Collections yet, you can add them at the <a href=@mycollections>My Collections</a> page.', array('@mycollections' => url('mycollections')))
      . '<br /><br /></p>',
    );
  }

  return $form;
}

/**
 * The access form.
 *
 * @param $form
 *   The drupal form array.
 * @param $values
 *   The values of the acl form element.
 *
 * @return
 *   Returns the form array.
 */
function _mediadrive_bulk_upload_access_acl_settings($form, $values) {
  // Creating the additional form elements.
  $form['control'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access control'),
  );

  $form['control']['acl_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain'),
    '#required' => FALSE,
    '#description' => t('Define which domains can view this video. Example; mediamosa.org, example.com'),
    // '#default_value' => self::default_value($values, 'acl_domain'),
    '#default_value' => !isset($values['acl_domain']) ? '' : $values['acl_domain'],
  );

  $form['control']['acl_realm'] = array(
    '#type' => 'textfield',
    '#title' => t('By realm'),
    '#required' => FALSE,
    '#description' => t('Select wich registered users based on their e-mail domain have access to this video. Example; @mediamosa.org, @example.com'),
    '#default_value' => !isset($values['acl_realm']) ? '' : $values['acl_realm'],
  );

  $form['control']['acl_user'] = array(
    '#type' => 'textfield',
    '#title' => t('By email'),
    '#required' => FALSE,
    '#description' => t('Select which registered users based on their e-mail adress have access to this video. Example; info@mediamosa.org, name@example.com'),
    // '#default_value' => self::default_value($values, 'acl_user'),
    '#default_value' => !isset($values['acl_user']) ? '' : $values['acl_user'],
  );

  return $form;
}
