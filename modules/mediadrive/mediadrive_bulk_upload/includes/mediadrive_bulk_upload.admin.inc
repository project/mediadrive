<?php
/**
 * @file
 * Provides an interface for the upload of Video files to Acquia Media Drive.
 */

/**
 * Implements the admin settings for this module.
 */
function mediadrive_bulk_upload_admin_settings($form_state) {
  // Obtain the saved values from the database.
  $mediadrive_bulk_upload = variable_get('mediadrive_bulk_upload', array(
    'video_path' => 'mediadrive_file_uploads',
    'maxfilesize' => '267386880',
  ));

  // Creating the settings form.
  $form = array();
  $form['filesystem'] = array(
    '#type' => 'fieldset',
    '#title' => t('Storage Settings for assets bulk upload'),
  );

  $form['filesystem']['video_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Server Upload Directory'),
    '#required' => FALSE,
    '#default_value' => isset($mediadrive_bulk_upload['video_path']) ? $mediadrive_bulk_upload['video_path'] : 'mediadrive_file_uploads',
    '#description' => t('Enter the directory (relative to the \'files\' directory) that will be used to temporally store video file uploads. These files will be uploaded to the Acquia Media Drive Server in the background.'),
  );

  $form['filesystem']['maxfilesize'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum allowed file size (in Bytes)'),
    '#required' => TRUE,
    '#default_value' => isset($mediadrive_bulk_upload['maxfilesize']) ? $mediadrive_bulk_upload['maxfilesize'] : '267386880',
    '#description' => t('Enter maximum size of a file that will be allowed for upload to Acquia Media Drive. The size units should be in bytes (Defaults to 267386880 bytes = 255MB).'),
  );


  return system_settings_form($form);
}

/**
 * Performs basic validation of data inserted by the administrator.
 */
function mediadrive_bulk_upload_admin_settings_validate(&$form, &$form_state) {
  // Validating that path does not have points.
  if (strpbrk($form_state['values']['video_path'], "\\/?%*:|\"<>")) {
    form_set_error('video_path', t('Enter a valid directory path. It should not contain the following characters: \' \ / ? % * : | " < > .\' in the name.'));
  }
  if (!is_numeric($form_state['values']['maxfilesize']) || $form_state['values']['maxfilesize'] <= 0) {
    form_set_error('maxfilesize', t('Enter a valid number for the maximum allowed file size.'));
  }

  $mediadrive_bulk_upload = array(
    'video_path' => check_plain($form_state['values']['video_path']),
    'maxfilesize' => $form_state['values']['maxfilesize'],
  );
  $form_state['values']['mediadrive_bulk_upload'] = $mediadrive_bulk_upload;
}
