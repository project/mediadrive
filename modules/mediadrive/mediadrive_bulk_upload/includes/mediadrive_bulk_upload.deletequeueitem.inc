<?php
/**
 * @file
 * Provides a function for deleting items from the queue.
 */

/**
 * Deletes an item from the queue.
 *
 * This function will be executed through an ajax call to delete
 * an item in the queue and be processed later.
 */
function mediadrive_bulk_upload_delete_queue_item() {
  // Verifying that the request is coming from ajax.
  $type = isset($_POST['type']) ? $_POST['type'] : '';
  $mediadrive_token = isset($_POST['mediadrive_token']) ? $_POST['mediadrive_token'] : '';
  $item_id = isset($_POST['item_id']) ? $_POST['item_id'] : '';
  if ($type == 'ajax' && drupal_valid_token($mediadrive_token, 'pR0t3cT3Dm3DiA') && is_numeric($item_id)) {
    // Remove item from the mediadriveBulkUpload queue.
    // It will ONLY be removed if it has NOT yet been claimed.
    $queue = DrupalQueue::get('mediadrivebulkupload');
    $success = $queue->removeItem($item_id);
    drupal_json_output($success);
  }
  else {
    // This is supposed to run on js only.
    drupal_json_output(FALSE);
  }
  drupal_exit();
}
