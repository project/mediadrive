<?php

/**
 * Implements hook_drush_help().
 */
function mediadrive_bulk_upload_drush_help($command) {
  switch ($command) {
    case 'drush:mediadrive-run-queue':
      return dt('Run the upload queue to send video files to Acquia Media Drive.');
    case 'drush:mediadrive-run-queue-item':
      return dt('Run the upload queue for only one queue item to send video files to Acquia Media Drive.');
  }
}

/**
 * Implements hook_drush_command().
 */
function mediadrive_bulk_upload_drush_command() {
  $items = array();
  $items['mediadrive-run-queue-item'] = array(
    'description' => dt('Run the upload queue for one single queue item to send a video file to Acquia Media Drive.'),
    'arguments'   => array(
      'item_id'    => dt('Used for a particular item_id. The function does nothing if no item_id is given.'),
    ),
    'examples' => array(
      'drush mediadrive-run-queue-item <item_id>' => 'Run the upload queue to send video files to Acquia Media Drive for  particular queue item.',
    ),
    'aliases' => array('md-rqi'),
  );
  $items['mediadrive-run-queue'] = array(
    'description' => dt('Run the Media Drive Bulk Upload.'),
    'examples' => array(
      'drush mediadrive-run-queue' => 'Run the upload command to bulk upload video files to Acquia Media Drive.',
    ),
    'aliases' => array('md-rq'),
  );
  return $items;
}

/**
 * Drush Callback.
 */
function drush_mediadrive_bulk_upload_mediadrive_run_queue_item($item_id = 'none') {
  if ($item_id != 'none') {
    // Read the MEDIAMOSA Bulk Upload Queue.
    $queue = DrupalQueue::get('mediadrivebulkupload');
    // Obtain the item that was claimed for processing.
    $item = $queue->getItem($item_id);
    if (!empty($item)) {
      // Set the Subscription.
      MEDIADRIVEMultitenant::setSubscriptionId($item['data']['sub']);
      // Transfer file.
      $success = _mediadrive_bulk_upload_run_queue_item($item);
      if ($success) {
        // Delete the item from the queue.
        watchdog('MEDIAMOSA Bulk Upload', 'File successfully uploaded to Acquia Media Drive: %filename', array('filename' => $item['data']['filename']), WATCHDOG_NOTICE);
      }
    }
    else {
      // File wasn't uploaded...
      drush_log(dt('The queue item_id given did not match. Available downloads are: !ds', array('!ds' => implode(', ', $names))), 'error');
    }
  }
  else {
    // item_id == 'none'.
    drush_log(dt('This download does not exist. Available downloads are: !ds', array('!ds' => implode(', ', $names))), 'error');
  }
}

/**
 * Runs the upload function for an item in the queue.
 */
function _mediadrive_bulk_upload_run_queue_item($item) {
  // The 'metadata' field in $item['data'] overrides any settings
  // obtained from the data, because those have been changed in the
  // interface by the user.
  $mediadrive_bulk_upload = variable_get('mediadrive_bulk_upload', array(
    'video_path' => 'mediadrive_file_uploads',
    'maxfilesize' => '267386880',
  ));
  $video_path = $mediadrive_bulk_upload['video_path'];
  $video_directory = drupal_realpath(file_default_scheme() . '://') . '/' . (isset($video_path) ? $video_path : 'mediadrive_file_uploads');
  $filename = $video_directory . '/' . $item['data']['filename'];

  // Creating the file object.
  $file_upload = new stdClass();
  $file_upload->uri = $filename;
  $file_upload->filename = $item['data']['filename'];
  $file_upload->name = basename($item['data']['filename']);

  // Run function for posting metadata to Acquia Media Drive.
  $metadata = array(
    'dc_title' => (isset($item['data']['metadata']['title']) ? $item['data']['metadata']['title'] : $item['data']['filename']),
    'mycollections' =>  (isset($item['data']['collections']) ? $item['data']['collections'] : array()),
  );
  // Creating the new upload class.
  $options = array(
    'create_still' => TRUE,
    'still_start_time' => $item['data']['still_start_time'],
    'transcode' => $item['data']['transcode_profiles'],
  );
  $permissions = empty($item['data']['acl']) ? array() : $item['data']['acl'];
  $upload_file_item = new MediaDriveUpload(
    $file_upload,
    $metadata,
    $permissions,
    $options,
    $item['data']['user_id']
  );
  if ($upload_file_item->send($options)) {
    if (!file_unmanaged_delete($file_upload->filename)) {
      watchdog('MEDIAMOSA Bulk Upload', 'File cannot be deleted: %filename', array('filename' => $item['data']['filename']), WATCHDOG_CRITICAL);
    }
    else {
      return TRUE;
    }
  }
  else {
    watchdog('MEDIAMOSA Bulk Upload', 'File failed to upload to Acquia Media Drive: %filename', array('filename' => $item['data']['filename']), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Sends the video file to Acquia Media Drive for only one item in the queue.
 */
function drush_mediadrive_bulk_upload_mediadrive_run_queue() {
  // Read the MEDIAMOSA Bulk Upload Queue.
  $queue = DrupalQueue::get('mediadrivebulkupload');
  $count = $queue->numberOfItems();
  drush_log('Total number of items: ' . $count);
  // Run the queue for all elements of the queue.
  for ($i = 0; $i < $count; $i++) {
    $item = $queue->claimItem();
    drush_log('Processing item: ' . $item->item_id);
    if (!empty($item)) {
      $success = drush_invoke_process('@self', 'mediadrive-run-queue-item', array('item_id' => $item->item_id), array('user' => $item->data['uid']));
      if ($sucess['error_status'] == 0) {
        $queue->deleteItem($item);
      }
      else {
        // Add drush_log
      }
    }
    else {
      // File wasn't uploaded...
      // Let the lease end so that another process can take it.
      watchdog('MEDIAMOSA Bulk Upload', 'No items to process in the queue.', array(), WATCHDOG_NOTICE);
    }
  }
}
