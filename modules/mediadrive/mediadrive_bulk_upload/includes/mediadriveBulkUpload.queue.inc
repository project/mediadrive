<?php
/**
 * @file
 * This file implements a customized queue class for bulk uploads.
 */

/**
 * MEDIAMOSABulkUploadQueue allows modification of queue items.
 *
 * Implements an additional update function to change the data
 * of the items that are stored in the system queue.
 */
class MediaDriveBulkUploadQueue extends SystemQueue {

  /**
   * Overrides createItem.
   *
   * Redefines createItem function to return the
   * last inserted item_id in the queue.
   */
  public function createItem($data) {
    // During a Drupal 6.x to 7.x update, drupal_get_schema() does not contain
    // the queue table yet, so we cannot rely on drupal_write_record().
    $query = db_insert('queue')
      ->fields(array(
        'name' => $this->name,
        'data' => serialize($data),
        // We cannot rely on REQUEST_TIME because many items might be created
        // by a single request which takes longer than 1 second.
        'created' => time(),
      ));
    // Return the $item_id, not TRUE/FALSE as in the SystemQueue.
    return $query->execute();
  }

  /**
   * Returns the item given the item_id.
   *
   * Used to obtain information about a specific item without claiming it.
   * For this to work, the item has to be already claimed.
   */
  public function getItem($item_id) {
    $query = db_select('queue', 'q')
      ->fields('q')
      ->condition('expire', 0, '!=')
      ->condition('item_id', $item_id, '=');
    $return_item = $query->execute()->fetchAssoc();
    $return_item['data'] = unserialize($return_item['data']);
    return $return_item;
  }

  /**
   * Implements a new function to update a queue item once
   * it is already in the queue.
   */
  // public function updateItem($item_id, $data) {
  //   $query = db_update('queue')
  //     ->fields(array(
  //       'data' => serialize($data),
  //     ))
  //     ->condition('item_id', $item_id);
  //   return $query->execute();
  // }

  /**
   * Removes an item from the queue (in any position).
   *
   * Implements a delete function considering that we do have
   * the item_id (not necessarily the last one in the queue).
   * The item can ONLY be deleted if it has NOT been claimed
   * by another process.
   */
  public function removeItem($item_id) {
    $num_deleted = db_delete('queue')
      ->condition('item_id', $item_id)
      ->condition('expire', 0, '=')
      ->execute();
    return (bool) $num_deleted;
  }
}
