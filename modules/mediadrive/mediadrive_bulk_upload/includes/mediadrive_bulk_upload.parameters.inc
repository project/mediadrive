<?php
/**
 * @file
 * Provides an interface for the upload of Video files to Acquia Media Drive.
 */

/**
 * Implements the admin settings for this module.
 */
function mediadrive_bulk_upload_parameters_form($form_state) {
  // Include the file for the generation of form elements.
  module_load_include('inc', 'mediadrive_bulk_upload', 'includes/mediadrive_bulk_upload.parameters.form');

  // Obtain the saved values from the database.
  $mediadrive_bulk_upload_params = _mediadrive_bulk_upload_session_get('mediadrive_bulk_upload_params', array());

  // Creating the settings form.
  $form = array();
  $form = _mediadrive_bulk_upload_transcode_settings($form, (empty($mediadrive_bulk_upload_params['transcode_profiles']) ? array() : $mediadrive_bulk_upload_params['transcode_profiles']));
  $form = _mediadrive_bulk_upload_still_settings($form, (empty($mediadrive_bulk_upload_params['still_start_time']) ? '00:00' : $mediadrive_bulk_upload_params['still_start_time']));
  $form = _mediadrive_bulk_upload_collections_settings($form, (empty($mediadrive_bulk_upload_params['collections']) ? '' : $mediadrive_bulk_upload_params['collections']));
  $form = _mediadrive_bulk_upload_access_acl_settings($form, (empty($mediadrive_bulk_upload_params['acl']) ? '' : $mediadrive_bulk_upload_params['acl']));
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit bulk upload parameters'),
  );
  return $form;
}

/**
 * Performs basic validation of data inserted by the administrator.
 */
function mediadrive_bulk_upload_parameters_form_validate(&$form, &$form_state) {
  // Performing validations functions to the form_state.
  // Check if still time is correct format.
  if (!preg_match("/[0-9]{1,2}:[0-9]{1,2}/", $form_state['values']['still_start_time'])) {
    form_set_error('still_start_time', t('Time for still generation must be in format MM:SS.'));
  }

  // removing the non-selected transcode profiles
  foreach ($form_state['values']['profiles'] as $key => $value) {
    if (!$value) {
      unset($form_state['values']['profiles'][$key]);
    }
  }
  // Defining the mediadrive_bulk_upload variable that will be saved in the database.
  $mediadrive_bulk_upload_params = array(
    // 'video_path' => check_plain($form_state['values']['video_path']),
    'transcode_profiles' => $form_state['values']['profiles'],
    'default_transcode_profile' => $form_state['values']['default_profile'],
    'still_start_time' => isset($form_state['values']['still_start_time']) ? $form_state['values']['still_start_time'] : '00:10',
    'collections' => isset($form_state['values']['mycollections']) ? $form_state['values']['mycollections'] : '',
    'acl' => array(
      'acl_domain' => isset($form_state['values']['acl_domain']) ? check_plain($form_state['values']['acl_domain']) : '',
      'acl_realm' => isset($form_state['values']['acl_realm']) ? check_plain($form_state['values']['acl_realm']) : '',
      'acl_user' => isset($form_state['values']['acl_user']) ? check_plain($form_state['values']['acl_user']) : '',
    ),
    'user_id' => mediamosa_ck::session_user_id(),
  );
  $form_state['values']['mediadrive_bulk_upload_params'] = $mediadrive_bulk_upload_params;

}

/**
 * Implements hook_form_submit().
 */
function mediadrive_bulk_upload_parameters_form_submit(&$form, &$form_state) {
  // Save upload parameters in the session variable.
  _mediadrive_bulk_upload_session_set('mediadrive_bulk_upload_params', $form_state['values']['mediadrive_bulk_upload_params']);

  // Go to the upload page.
  drupal_goto('asset/bulk_upload/upload');
}
