<?php
/**
 * @file
 * Provides an interface for the upload of Video files to Acquia Media Drive.
 */

/**
 * Implements an upload handler that supports XHR and CORS.
 *
 * We are using XHR to allow for the possibility to upload big files
 * without restrictions, independently of the php.ini configuration settings.
 * Drupal default options is restricted to php.ini settings.
 */
function mediadrive_bulk_upload_file_upload() {
  global $base_url;
  $mediadrive_token = isset($_POST['mediadrive_token']) ? $_POST['mediadrive_token'] : '';
  $file = isset($_POST['file']) ? $_POST['file'] : '';
  $is_delete_request = $_SERVER['REQUEST_METHOD'] == 'DELETE';
  $referer = $_SERVER['HTTP_REFERER'];
  $accept_deletion = $is_delete_request && (strpos($referer, $base_url . '/asset/bulk_upload/upload') !== FALSE);
  if (drupal_valid_token($mediadrive_token, 'pR0t3cT3Dm3DiA') || $accept_deletion) {
    // Making sure the video path exists and it is writable.
    $mediadrive_bulk_upload = variable_get('mediadrive_bulk_upload', array(
      'video_path' => 'mediadrive_file_uploads',
      'maxfilesize' => '267386880',
    ));
    $video_path = $mediadrive_bulk_upload['video_path'];
    $video_directory = drupal_realpath(file_default_scheme() . '://') . '/' . (isset($video_path) ? $video_path : 'mediadrive_file_uploads');
    $files_path = variable_get('file_' . file_default_scheme() . '_path', conf_path() . '/files');
    file_prepare_directory($video_directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    $sub_id = MEDIADRIVEMultitenant::getSubscriptionId();
    // Setting up the Options.
    $options = array(
      'upload_dir' => $video_directory . '/',
      'script_url' => '/' . current_path() . '?s=' . $sub_id,
      // 'download_url' => file_directory_path() . '/mediadrive_file_uploads/',
      'upload_url' => $files_path . '/' . (isset($video_path) ? $video_path . '/' : 'mediadrive_file_uploads/'),
      'access_control_allow_origin' => FALSE,
      // 'delete_type' => 'POST',
    );
    $upload_handler = new UploadHandler($options);
  }
  else {
    return FALSE;
  }
}
