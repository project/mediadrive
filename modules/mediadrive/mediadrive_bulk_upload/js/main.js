(function ($) {
 'use strict';
  /**
   * Obtains the value of a url parameter.
   */
  function getURLParameter(url, name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(url)||[,""])[1].replace(/\+/g, '%20'))||null;
  }

  /**
   * Adds a new item in the Queue.
   */
  function addFileToQueue(filename) {
    var item_id;
    var index_id;
    $.each(Drupal.settings.files, function (index, obj) {
      if (Drupal.settings.files[index].filename == filename) {
        index_id = index;
        return false;
      }
    });
    jQuery.ajax({
      type: "POST",
      async: false, // use sync results
      url: 'add_queue_item?s=' + Drupal.settings.sub_id,
      data: {
        'type': 'ajax',
        'mediadrive_token': Drupal.settings.mediadrive_token,
        'id': filename,
        'sub': Drupal.settings.sub_id,
        'metadata': Drupal.settings.files[index_id]
      },
      dataType: "json",
      success: function (data) {
        if (data) {
          item_id = data;
        }
      }
    });
    return item_id;
  }

  /**
   * Removes an item from the Queue.
   */
  function removeFileFromQueue(item_id) {
    jQuery.ajax({
      type: "POST",
      async: false, // use sync results
      url: 'delete_queue_item?s=' + Drupal.settings.sub_id,
      data: {
        'type': 'ajax',
        'mediadrive_token': Drupal.settings.mediadrive_token,
        'item_id': item_id
      },
      dataType: "json",
      success: function (data) {
        if (data) {
          item_id = data;
        }
        else {
          alert('File is being transferred and cannot be deleted...');
          return false;
        }
      }
    });
    return item_id;
  }

  /**
   * Adds a new entry to the control array.
   */
  function addFileData(filename) {
    var obj = {
      'filename': filename,
    }
    Drupal.settings.files.push(obj);
  }

  /**
   * Adds a new property to an item in the control array.
   */
  function addFileDataItem(filename, property_name, property_value) {
    $.each(Drupal.settings.files, function (index, obj) {
      if (Drupal.settings.files[index].filename == filename) {
        Drupal.settings.files[index][property_name] = property_value;
      }
    });
  }

  /**
   * Removes an item from the control array and from the Queue.
   */
  function removeFileDataItem(filename) {
    $.each(Drupal.settings.files, function (index, obj) {
      if (Drupal.settings.files[index].filename == filename) {
        var removeItem = Drupal.settings.files[index];
        var success = removeFileFromQueue(removeItem.item_id);
        if (success) {
          Drupal.settings.files = jQuery.grep(Drupal.settings.files, function(value) {
            return value != removeItem;
          });
        }
      }
    });
  }

  Drupal.behaviors.mediadriveBulkUploadForm = {
    attach: function (context, settings) {
      // Attach behaviors to the different actions of the file:
      $('#mediadrive-bulk-upload-uploadform').fileupload({
          acceptFileTypes: "/(\.|\/)(" + Drupal.settings.allowed_extensions + ")$/i",
          previewMaxWidth: 120,
          maxChunkSize: 10000000, // 10 MB
          maxFileSize: Drupal.settings.maxfilesize,
          // This is triggered when you drag'n'drop a file over the uploader
          drop: function (e, data) {
            $.each(data.files, function (index, file) {
              // Do something when dragging and dropping a file in the component.
              // addFileData(data.files[index].name);
            });
          },
          // This is triggered when you click a button and select files from a list.
          change: function (e, data) {
            $.each(data.files, function (index, file) {
              // Do something after a file is added through a file selection.
              // addFileData(data.files[index].name);
            });
          },
          // This is triggered on every file in the queue when you click "Upload"
          submit: function (e, data) {
            // Do something with the file being uploaded.
          },
          // This is triggered on every file in the queue when you click "Upload"
          finished: function (e, data) {
            // Do something with the uploaded file.
            if (typeof(data.result) !== 'undefined') {
              var filename = data.result.files[0].name;
              addFileData(filename);
              var item_id = addFileToQueue(filename);
              addFileDataItem(filename, 'item_id', item_id);
            }
          },
          // This is triggered on every file in the queue when you click "Cancel"
          // failed: function (e, data) {
            // Do something with when the file is being cancelled.
            // alert(data.files[0].name);
          // },
          destroyed: function (e, data) {
            // Do something with when the file is being deleted.
            // console.log(data);
            var filename = getURLParameter(data.url, 'file');
            // console.log(filename);
            removeFileDataItem(filename);
          }
      });
      // Make the actions show up only when files are selected
      // Quirky, should revisit.
      $('.submit-actions').hide();
      $('span.fileinput-button').click(function(e){
        $('.submit-actions').show();
      });
      $('button.cancel').click(function(e){
        $('.submit-actions').hide();
      });
      $('td.cancel .btn-warning').live('click',function(e){
        if($(this).parents('tr').siblings().size() == 0) {
          $('.submit-actions').hide();
        }
      });

    }
  }

})(jQuery);
