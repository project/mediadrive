Bootstrap Libraries that work with jQuery-File-Upload.

CSS:
---
- http://blueimp.github.com/cdn/css/bootstrap.min.css
- http://blueimp.github.com/cdn/css/bootstrap-responsive.min.css
- http://blueimp.github.com/Bootstrap-Image-Gallery/css/bootstrap-image-gallery.min.css

JSS:
---
- http://blueimp.github.com/JavaScript-Templates/tmpl.min.js
- http://blueimp.github.com/JavaScript-Load-Image/load-image.min.js
- http://blueimp.github.com/JavaScript-Canvas-to-Blob/canvas-to-blob.min.js
- http://blueimp.github.com/cdn/js/bootstrap.min.js
- http://blueimp.github.com/Bootstrap-Image-Gallery/js/bootstrap-image-gallery.min.js

