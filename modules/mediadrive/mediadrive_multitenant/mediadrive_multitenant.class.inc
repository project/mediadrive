<?php

class MEDIADRIVEMultitenant {

  /**
   * Holds context of current subscription.
   *
   * @var int
   */
  static protected $sub_id;
  /**
   * Paths that redirect. All paths that should require a subscription context.
   *
   * @array
   */
  static public $paths_that_redirect = array(
    'asset',
    'assets',
    'tos',
    'collection',
    'collections',
    'mycollections',
    'search',
    'settings',
    '<front>'
  );

  /**
   * Returns the connector object.
   *
   * @param  $user Drupal user object.
   */
  static public function getUserConnector($user) {
    if($sub = self::getUserCurrentSubscription($user->uid)) {
      $connection = self::getSubscriptionConnectionData($sub);
      return new MediaMosaCkConnectorWrapper($connection['username'][0]['value'],
                                       $connection['password'][0]['value'],
                                       $connection['url'][0]['value']);
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns the subscription settings nodes.
   *
   * @param  $sub_id Subscription id.
   */
  static public function getSubscriptionSettingsNodes($sub_id) {
    $query = new EntityFieldQuery();
    $result = $query
      ->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'subscription_settings')
      ->propertyCondition('status', 1)
      ->fieldCondition('field_subscription_reference', 'nid', $sub_id, '=')
      ->execute();
      if ($result) {
        return $setting_nodes = array_keys($result['node']);
      }
      else {
        return FALSE;
      }
  }

  /**
   * Returns a list of subscriptions this user belongs to.
   *
   * @param  $uid User id.
   */
  static public function getUserSubscriptions($uid) {
    try {
      $query = new EntityFieldQuery();
      $result = $query
        ->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'user_reference')
        ->propertyCondition('status', 1)
        ->fieldCondition('field_user_reference_user', 'uid', $uid, '=')
        ->execute();
      if ($result) {
        $relationship_nodes = node_load_multiple(array_keys($result['node']));
        $sub_ids = array();
        foreach ($relationship_nodes as $relationship_node) {
          $sub_ids[] = array(
            'sub_id' => field_get_items('node', $relationship_node, 'field_user_reference_sub', $relationship_node->language),
            'contact_type' => field_get_items('node', $relationship_node, 'field_user_reference_type', $relationship_node->language),
          );
        }
        return $sub_ids;
      }
      else {
        return FALSE;
      }
    }
    catch (Exception $e) {
      return FALSE;
    }
  }

  /**
   * Returns a simplified list of subscriptions this user belongs to.
   *
   * @param  $uid User id.
   */
  static public function getUserSubscriptionsSimpleFormat($uid) {
    $subs = self::getUserSubscriptions($uid);
    if (!$subs) {
      return FALSE;
    }
    $ids = array();
    foreach($subs as $sub) {
      $ids[] = $sub['sub_id'][0]['nid'];
    }
    return $ids;
  }

  /**
   * Returns the current user subscription.
   *
   * @param  $uid User id.
   */
  static public function getUserCurrentSubscription($uid) {
    if($subs = self::getUserSubscriptionsSimpleFormat($uid)) {
      foreach ($subs as $sub) {
        if ($sub == self::getSubscriptionId()) {
          return $sub;
        }
      }
    }
    return FALSE;
  }

  /**
   * Returns the current user subscription settings.
   *
   * @param  $sub_id Subscription id.
   */
  static public function getUserCurrentSubscriptionSettings($sub_id) {
    /**
     * HERE: need to work some logic to pair the subs results with the contextual
     * subscription from PURL.
     *
     * For now, just hardcoding to first item in array.
     */
    $setting_nodes = self::getSubscriptionSettingsNodes($sub_id);
    if ($setting_nodes) {
      $node = node_load($setting_nodes[0]);
    }
    else {
      global $user;
      $node = new stdClass();
      $node->type = 'subscription_settings';
      node_object_prepare($node);
      $node->uid = $user->uid;
      $node->name = $user->name;
      $node->language = 'und';
      $node->title = 'Settings for subscription ' . $sub_id;
      $node->status = 1;
      $node->promote = 0;
      $node->revision = 0;
      $node->changed = $_SERVER['REQUEST_TIME'];
      $node->created = $_SERVER['REQUEST_TIME'];
      $node->field_subscription_reference[$node->language][0]['nid'] = $sub_id;
      node_submit($node);
      node_save($node);
      $node = node_load($node->nid);
    }
    return array(
      'settings_id' => $node->nid,
      'settings_still_type' => field_get_items('node', $node, 'field_settings_still_type', $node->language),
      'settings_still_size' => field_get_items('node', $node, 'field_settings_still_size', $node->language),
      'settings_still_h_padding' => field_get_items('node', $node, 'field_settings_still_h_padding', $node->language),
      'settings_still_v_padding' => field_get_items('node', $node, 'field_settings_still_v_padding', $node->language),
      'settings_still_time_code' => field_get_items('node', $node, 'field_settings_still_time_code', $node->language),
      'settings_still_per_file' => field_get_items('node', $node, 'field_settings_still_per_file', $node->language),
      'settings_still_every_sec' => field_get_items('node', $node, 'field_settings_still_every_sec', $node->language),
      'settings_still_start_time' => field_get_items('node', $node, 'field_settings_still_start_time', $node->language),
      'settings_still_end_time' => field_get_items('node', $node, 'field_settings_still_end_time', $node->language),
      'settings_transcode_default' => field_get_items('node', $node, 'field_settings_transcode_default', $node->language),
      'settings_transcode_allowed' => field_get_items('node', $node, 'field_settings_transcode_allowed', $node->language),
    );
  }

  /**
   * Saves settings to the right settings node.
   *
   * @param  $settings_id Settings node id.
   */
  static public function setSubscriptionValues($values) {
    $node = node_load($values['sub_id']);

    foreach ($values as $key => $value) {
      $property = 'field_' . $key;
      if (property_exists($node, $property)) {
        $node->{$property}[$node->language][0]['value'] = $value;
      }
    }
    try {
      node_save($node);
      return TRUE;
    }
    catch (Exception $e) {
      return FALSE;
    }
  }

  /**
   * Saves settings to the right settings node.
   *
   * @param  $settings_id Settings node id.
   */
  static public function setSubscriptionSettings($settings) {
    $node = node_load($settings['settings_id']);

    foreach ($settings as $key => $setting) {
      $property = 'field_' . $key;
      if (property_exists($node, $property)) {
        $node->{$property}[$node->language][0]['value'] = $setting;
      }
    }
    try {
      node_save($node);
      return TRUE;
    }
    catch (Exception $e) {
      return FALSE;
    }
  }

  /**
   * Returns the connection data for this user.
   *
   * @param  $sub_id Subscription id.
   */
  static public function getSubscriptionConnectionData($sub_id) {
    $node = node_load($sub_id);
    if ($node->type == 'subscription') {
      return array(
        'url' => field_get_items('node', $node, 'field_mediamosa_connector_url', $node->language),
        'username' => field_get_items('node', $node, 'field_mediamosa_connector_user', $node->language),
        'password' => field_get_items('node', $node, 'field_mediamosa_connector_pass', $node->language),
      );
    }
    else {
      throw new Exception("Not a valid subscription node", 1);
    }
  }

  /**
   * Sets a cookie related to MEDIADRIVE.
   * Wrapper to prefix cookie names.
   *
   * @param  $name Cookie name
   * @param  $value Cookie value
   * @param  $expire Cookie expiration. Defaults to 24 hs
   */
  static public function setCookie($name, $value, $expire = NULL) {
    $expire = empty($expire) ? (time() + 60*60*24) : $expire;
    return setrawcookie('mediadrive.' . $name, $value, $expire);
  }

  /**
   * Gets a MEDIADRIVE cookie.
   *
   * @param  $name Cookie name
   */
  static public function getCookie($name) {
    if (strpos($name, 'mediadrive.') === 0) {
      $cookie = $name;
    }
    else {
      $cookie = 'mediadrive.' . $name;
    }
    return isset($_COOKIE[$cookie]) ? $_COOKIE[$cookie] : FALSE;
  }

  /**
   * Get current subscription node.
   *
   * @throws Exception
   *   If missing subscription id context.
   *
   * @return
   *   Node object
   */
  static public function getSubscription() {
    $nid = self::getSubscriptionId();

    if (empty($nid)) {
      throw new Exception(t('Missing subscription id context.'));
    }

    return node_load($nid);
  }

  /**
   * Gets the subscription ID from the PURL context.
   *
   * @return int
   *   Subscipriont node id
   */
  static public function getSubscriptionId() {
    return !empty(self::$sub_id) ? self::$sub_id : NULL;
  }

  /**
   * Saves subscription context.
   *
   * @param int $sub_id
   *   Subscription node id
   */
  static public function setSubscriptionId($sub_id) {
    self::$sub_id = $sub_id;
  }
}
