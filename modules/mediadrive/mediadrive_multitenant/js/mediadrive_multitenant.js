(function($) {
  // Behavior to make the submit button dissapear.
  Drupal.behaviors.mediadriveSubscriptionSelectionNoButton = {
    attach: function (context) {
      var form = $('#mediadrive-sub-form'), select = $('.mediadrive-onchange-submit');
      form.find('.form-submit').hide();

      // With only one option we click for the user.
      if(select.find('option').size() <= 2) {
        select.find('option').each(function(e){
          if($(this).val() != "") {
            select.removeClass('mediadrive-onchange-submit');
            $(this).attr("selected","selected");
            form.submit();
            select.attr('disabled', 'disabled');
          }
        });
      }

      // Send form on select change.
      select.change(function(e){
        $(this).removeClass('mediadrive-onchange-submit');
        form.submit();
      });
    }
  };
})(jQuery);


