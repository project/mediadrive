<?php
/**
 * @file
 * Creates a page that lists all tags.
 */

/**
 * Creates a page that list all metadata_tags associated with assets.
 */
function mediadrive_autocomplete_tags_list() {
  // Obtain the list of metadata tags from Media Drive.
  $metadata_tags = new mediadrive_sdk_metadata_tags();
  $asset_tags = $metadata_tags->get();
  $links = $metadata_tags->decode($asset_tags, 'link');
  drupal_add_css(drupal_get_path('module', 'mediadrive_autocomplete_tags') . '/css/mediadrive_autocomplete_tags.list.css');
  return theme('mediadrive_autocomplete_tags_list', array('links' => explode(',', $links)));
}
