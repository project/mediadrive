<?php
/**
 * @file mediadrive-autocomplete-tags-list.tpl.php
 * Template file for metadata_tags list.
 *
 * @ingroup mediadrive_autocomplete_tags_templates
 */
?>
<div class= "mediadrive-tags-list">
  <?php foreach ($links as $key => $link) { ?>
  <div class = "mediadrive-tags-element mediadrive-tags-element-<?php print $key ?>">
    <?php print ($key < count($links) - 1) ? $link . ", " : $link . "."; ?>
  </div>
  <?php } ?>
</div>
