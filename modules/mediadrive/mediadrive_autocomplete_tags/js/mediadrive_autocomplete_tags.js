
/**
 * @file:
 * Converts textfield to a mediadrive_autocomplete_tags widget.
 */

(function($) {
  Drupal.mediadrive_autocomplete_tags = Drupal.mediadrive_autocomplete_tags || {};

  Drupal.behaviors.mediadrive_autocomplete_tags = {
    attach: function(context) {
      var autocomplete_settings = Drupal.settings.mediadrive_autocomplete_tags;

      $('input.mediadrive-autocomplete-tags-form').once( function() {
        if (autocomplete_settings[$(this).attr('id')].multiple === true) {
          new Drupal.mediadrive_autocomplete_tags.MultipleWidget(this, autocomplete_settings[$(this).attr('id')]);
        } else {
          new Drupal.mediadrive_autocomplete_tags.SingleWidget(autocomplete_settings[$(this).attr('id')]);
        }
      });
    }
  };

  Drupal.mediadrive_autocomplete_tags.empty =  {label: '- ' + Drupal.t('None') + ' -', value: "" };

  /**
   * EscapeRegex function from jquery autocomplete, is not included in drupal.
   */
  Drupal.mediadrive_autocomplete_tags.escapeRegex = function(value) {
    return value.replace(/[-[\]{}()*+?.,\\^$|#\s]/gi, "\\$&");
  };

  /**
   * Filter function from jquery autocomplete, is not included in drupal.
   */
  Drupal.mediadrive_autocomplete_tags.filter = function(array, term) {
    var matcher = new RegExp(Drupal.mediadrive_autocomplete_tags.escapeRegex(term), "i");
    return $.grep(array, function(value) {
      return matcher.test(value.label || value.value || value);
    });
  };

  Drupal.mediadrive_autocomplete_tags.Widget = function() {
  };

  Drupal.mediadrive_autocomplete_tags.Widget.prototype.uri = null;

  /**
   * Allows widgets to filter terms.
   * @param term
   *   A term that should be accepted or not.
   * @return {Boolean}
   *   True if the term should be accepted.
   */
  Drupal.mediadrive_autocomplete_tags.Widget.prototype.acceptTerm = function(term) {
    return true;
  };

  Drupal.mediadrive_autocomplete_tags.Widget.prototype.init = function(settings) {
    if ($.browser.msie && $.browser.version === "6.0") {
      return;
    }

    this.id = settings.input_id;
    this.jqObject = $('#' + this.id);

    this.uri = settings.uri;
    this.multiple = settings.multiple;
    this.required = settings.required;

    var self = this;
    var parent = this.jqObject.parent();
    var parents_parent = this.jqObject.parent().parent();

    parents_parent.append(this.jqObject);
    parent.remove();
    parent = parents_parent;

    var generateValues = function(data, term) {
      var result = new Array();
      for (terms in data) {
        if (self.acceptTerm(terms)) {
          result.push({
            label: data[terms],
            value: terms
          });
        }
      }
      if ($.isEmptyObject(result)) {
        result.push({
          label: Drupal.t("The term '@term' will be added.", {'@term' : term}),
          value: term,
          newTerm: true
        });
      }
      return result;
    };

    var cache = {}
    var lastXhr = null;

    var obtainValues = function(request) {
      var tags = Drupal.settings.mediadrive_autocomplete_tags.tags;
      var term = request.term;
      var data = {};
      for (tag in tags) {
        if (tag.indexOf(term) != -1) {
          data[tag] = tag;
        }
      }
      if ($.isEmptyObject(data) && $.isEmptyObject(term)) {
        return tags;
      }
      else {
        return data;
      }
    }

    this.source = function(request, response) {
      var term = request.term;
      var data = obtainValues(request);
      response(generateValues(data, term));
      return response;
    }
    this.jqObject.autocomplete({
      'source' : this.source,
      'minLength': settings.min_length
    });

    this.jqObject.data("autocomplete")._renderItem = function(ul, item) {
      return $("<li></li>").data("item.autocomplete", item).append("<a>" + item.label + "</a>").appendTo(ul);
    };
  };

  Drupal.mediadrive_autocomplete_tags.Widget.prototype.generateValues = function(data) {
    var result = new Array();
    for (var index in data) {
      result.push(data[index]);
    }
    return result;
  };

  /**
   * Generates a single selecting widget.
   */
  Drupal.mediadrive_autocomplete_tags.SingleWidget = function(settings) {
    this.init(settings);
    this.setup();

  };

  Drupal.mediadrive_autocomplete_tags.SingleWidget.prototype = new Drupal.mediadrive_autocomplete_tags.Widget();

  Drupal.mediadrive_autocomplete_tags.SingleWidget.prototype.setup = function() {
    var jqObject = this.jqObject;
    var parent = jqObject.parent();

    parent.addClass('mediadrive-autocomplete-tags-single-container');

    parent.mousedown(function() {
      if (parent.hasClass('mediadrive-autocomplete-tags-single-open')) {
        jqObject.autocomplete('close');
      } else {
        jqObject.autocomplete('search', '');
      }
    });

    var arrow = $('<span class="mediadrive-autocomplete-tags-arrow ui-icon ui-icon-triangle-1-s">&nbsp;</span>').insertAfter(jqObject);

    jqObject.addClass('ui-corner-left');

    jqObject.bind( "autocompleteopen", function(event, ui) {
      arrow.removeClass('ui-icon-triangle-1-s')
      arrow.addClass('ui-icon-triangle-1-n');

      parent.addClass('mediadrive-autocomplete-tags-single-open');
    });

    jqObject.bind( "autocompleteclose", function(event, ui) {
      arrow.removeClass('ui-icon-triangle-1-n')
      arrow.addClass('ui-icon-triangle-1-s');
      parent.removeClass('mediadrive-autocomplete-tags-single-open');
    });
  };

  /**
   * Creates a multiple selecting widget.
   */
  Drupal.mediadrive_autocomplete_tags.MultipleWidget = function(input, settings) {
    this.init(settings);
    this.setup();
  };

  Drupal.mediadrive_autocomplete_tags.MultipleWidget.prototype = new Drupal.mediadrive_autocomplete_tags.Widget();
  Drupal.mediadrive_autocomplete_tags.MultipleWidget.prototype.items = new Object();


  Drupal.mediadrive_autocomplete_tags.MultipleWidget.prototype.acceptTerm = function(term) {
    // Accept only terms, that are not in our items list.
    return !(term in this.items);
  };

  Drupal.mediadrive_autocomplete_tags.MultipleWidget.Item = function (widget, item) {
    if (item.newTerm === true) {
      item.label = item.value;
    }

    this.value = item.value;
    this.element = $('<span class="mediadrive-autocomplete-tags-item">' + item.label + '</span>');
    this.widget = widget;
    this.item = item;
    var self = this;

    var close = $('<a class="mediadrive-autocomplete-tags-item-delete" href="javascript:void(0)"></a>').appendTo(this.element);
    // Use single quotes because of the double quote encoded stuff.
    var input = $('<input type="hidden" value=\'' + this.value + '\'/>').appendTo(this.element);

    close.mousedown(function() {
      self.remove(item);
    });
  };

  Drupal.mediadrive_autocomplete_tags.MultipleWidget.Item.prototype.remove = function() {
    this.element.remove();
    var values = this.widget.valueForm.val();
    var regex = new RegExp('( )*""' + this.item.value + '""|' + this.item.value + '( )*', 'gi');
    this.widget.valueForm.val(values.replace(regex, ''));
    delete this.widget.items[this.value];
  };

  Drupal.mediadrive_autocomplete_tags.MultipleWidget.prototype.setup = function() {
    var jqObject = this.jqObject;
    var parent = jqObject.parent();
    var value_container = jqObject.parent().parent().children('.mediadrive-autocomplete-tags-value-container');
    var value_input = value_container.children().children();
    var items = this.items;
    var self = this;
    this.valueForm = value_input;

    // Override the resize function, so that the suggestion list doesn't resizes
    // all the time.
    jqObject.data("autocomplete")._resizeMenu = function()  {};

    jqObject.show();
    value_container.hide();

    // Add the default values to the box.
    var default_values = value_input.val();
    default_values = $.trim(default_values);
    default_values = default_values.substr(2, default_values.length-4);
    default_values = default_values.split('"" ""');

    for (var index in default_values) {
      var value = default_values[index];
      if (value != '') {
        // If a terms is encoded in double quotes, then the label should have
        // no double quotes.
        var label = value.match(/["][\w|\s|\D|]*["]/gi) !== null ? value.substr(1, value.length-2) : value;
        var item = {
          label : label,
          value : value
        };
        var item = new Drupal.mediadrive_autocomplete_tags.MultipleWidget.Item(self, item);
        item.element.insertBefore(jqObject);
        items[item.value] = item;
      }
    }

    jqObject.addClass('mediadrive-autocomplete-tags-multiple');
    parent.addClass('mediadrive-autocomplete-tags-multiple');

    // Adds a value to the list.
    this.addValue = function(ui_item) {
      var item = new Drupal.mediadrive_autocomplete_tags.MultipleWidget.Item(self, ui_item);
      item.element.insertBefore(jqObject);
      items[ui_item.value] = item;
      var new_value = ' ""' + ui_item.value + '""';
      var values = value_input.val();
      value_input.val(values + new_value);
      jqObject.val('');
    };

    parent.mouseup(function() {
      jqObject.autocomplete('search', '');
      jqObject.focus();
    });

    jqObject.bind("autocompleteselect", function(event, ui) {
      self.addValue(ui.item);
      jqObject.width(25);
      // Return false to prevent setting the last term as value for the jqObject.
      return false;
    });

    jqObject.bind("autocompletechange", function(event, ui) {
      jqObject.val('');
    });

    jqObject.blur(function() {
      var last_element = jqObject.parent().children('.mediadrive-autocomplete-tags-item').last();
      last_element.removeClass('mediadrive-autocomplete-tags-item-focus');
    });

    var clear = false;

    jqObject.keydown(function (event) {
      var value = jqObject.val();
      // If a comma was entered and there is none or more then one comma,
      // then enter the new term.
      if (event.which == 188 && (value.split('"').length - 1) != 1) {
        value = value.substr(0, value.length);
        if (self.items[value] === undefined && value != '') {
          var ui_item = {
            label: value,
            value: value
          };
          self.addValue(ui_item);
        }
        clear = true;
      }

      // If the Backspace key was hit and the input is empty
      if (event.which == 8 && value == '') {
        var last_element = jqObject.parent().children('.mediadrive-autocomplete-tags-item').last();
        // then mark the last item for deletion or deleted it if already marked.
        if (last_element.hasClass('mediadrive-autocomplete-tags-item-focus')) {
          var value = last_element.children('input').val();
          self.items[value].remove(self.items[value]);
          jqObject.autocomplete('search', '');
        } else {
          last_element.addClass('mediadrive-autocomplete-tags-item-focus');
        }
      } else {
        // Remove the focus class if any other key was hit.
        var last_element = jqObject.parent().children('.mediadrive-autocomplete-tags-item').last();
        last_element.removeClass('mediadrive-autocomplete-tags-item-focus');
      }
    });


    jqObject.keydown(function (event) {
      if (clear) {
        jqObject.val('');
        clear = false;
        // Return false to prevent entering the last character.
        return false;
      }
      var cur_width = jqObject.width();
      jqObject.width(cur_width + 9);
    });
  };
})(jQuery);
