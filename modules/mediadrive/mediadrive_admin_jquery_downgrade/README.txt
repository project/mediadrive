As we decided to use Twitter Bootstrap as our base theme for MEDIADRIVE, we had to
install jquery_update to meet the theme's requirement of jQuery 1.7+ being
installed.

Unfortunatelly jQuery 1.5+ seems to break some functionallity on Views UI. This
module performs a quick downgrade of jQuery to its 1.5 version when loaded on
any admin path, thus ensuring Views and any other module relying on jQuery
doesn't find a compatibility issue on any admin page.
